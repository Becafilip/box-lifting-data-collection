%STEP03_V2_PROCESS script loads the processed data that has been grouped,
%interpolated, cleaned, and undergone Inverse Kinematics, it also loads the
%labeled segmentation given inside the raw data folder, within files whose 
%name ends with _UpsDownsSamples.xlsx, and just segments the data.
%Data is taken from:
%'../../processed_data/step02_extract_angles'
%The segmentation is taken from:
%'../../raw_data/subjects_data'
%And the results are saved twofold, in:
%   - '../../processed_data/step03a_v2_united_segmentation' where the 
%   information from the text files is loaded and saved as a cell array
%   alongised all the raw data. Nothing is thrown away.    
%   - '../../processed_data/step03b_v2_separated_segmentation' where each
%   repetition is saved in a separate .mat file in the form of a structure
%   called trial which has the fields
%       - q : Joint angles
%       - events : indices of different events in lifting motion
%       - modelParam : the segment lengths, masses, COMs, intertias, total
%       height and weight
%       - Markers : Contains all Markers from the data collection
%       - Forceplate : Contains all forces, moments and cop's measured by the
%       forceplate
%       - subject_name : 'subject_' + name
%       - session_name : the name of the session given by data collector
%       - set_number : the trials are grouped into sets, this is the number of
%       the set
%       - rep_number : the trials are grouped into sets, this is the number of
%       the trial within the set
clear all;
close all;
clc;

%% Add model functions
addpath('../model');
addpath('../other');

%% Define directories that are in play

raw_data_dir = '../../raw_data/subjects_data';
input_data_dir = '../../processed_data/step02_extract_angles';
processed_data_dir_a = '../../processed_data/step03a_v2_united_segmentation';
processed_data_dir_b = '../../processed_data/step03b_v2_separated_segmentation';
if ~exist(processed_data_dir_a, 'dir'); mkdir(processed_data_dir_a); end
if ~exist(processed_data_dir_b, 'dir'); mkdir(processed_data_dir_b); end

%% Some constants
column_set = 2;
% Times are t_lift	t1	t2	t3	t4	t_lift_end	t_lower	t5	t6	t7	t8	t_lower_end
column_t_lift = 5;

%% Check raw data directory

% Get all within raw_data folder (by default two directories with name '.'
% and '..', so we'll start checking items with index 3)
subject_data = dir(raw_data_dir);

% Total number of items within it
num_items = length(subject_data);

% Go from the first non-default element at index 3
for ii = 3 : num_items
% for ii = 3 : 3

    % If current item isn't a directory, skip this
    if ~subject_data(ii).isdir
        continue
    end
    
    % Get subdirectory name
    subdir_name = subject_data(ii).name;
    
    % Get the same directory name just from input data
    inputdir = [input_data_dir, '/', subdir_name];
    
    % Make the same subdirectory within the processed data directory
    savedir_a = [processed_data_dir_a, '/', subdir_name];
    savedir_b = [processed_data_dir_b, '/', subdir_name];
    if ~exist(savedir_a, 'dir'); mkdir(savedir_a); end
    if ~exist(savedir_b, 'dir'); mkdir(savedir_b); end
    
    % Get fullpath to subdirectory
    subdir_path = [raw_data_dir, '/', subdir_name];
    
    % Get all items within
    subdir_items = dir(subdir_path);
    
    % Go through subdirectory items to find labelings
    labelings = {};
    for jj = 1 : length(subdir_items)
        % Treat only those that end with .txt
        if endsWith(subdir_items(jj).name, '_UpsDownsSamples.xlsx', 'IgnoreCase', true)
            % Save their names
            labelings{end+1} = subdir_items(jj).name;
        end
    end
    
    %%  <Process labeling files>
    % Go through all files
    for jj = 1 : length(labelings)
%     for jj = 1 : 1
        % Get the current path to labeling file
        curr_labeling_file_path = [subdir_path, '/', labelings{jj}];
        % Get the name of the session
        session_name = erase(labelings{jj}, '_UpsDownsSamples.xlsx');
        
        % Get the current path to input file ( same name as labeling file but
        % without _UpsDownsSamples.xlsx and with .mat instead, in the input
        % directory)
        curr_input_file_path = [inputdir, '/', session_name, '.mat'];
        % Get the path to saving the output in united format
        curr_file_save_path_a = [savedir_a, '/', session_name, '.mat'];
        % Get the path to saving the output in separated format and create
        % corresponding directory
        curr_file_save_path_b = [savedir_b, '/', session_name];
        if ~exist(curr_file_save_path_b); mkdir(curr_file_save_path_b); end
        
        % Load current input
        curr_input = load(curr_input_file_path);
        
        % Bring each sample to the range [-pi, pi] except 5th joint which
        % stays in [0, 2*pi]
        curr_input.q = mod(curr_input.q, 2*pi);
        curr_input.q(curr_input.q > pi) = curr_input.q(curr_input.q > pi) - 2 * pi;
        curr_input.q(5, curr_input.q(5, :) < 0) = curr_input.q(5, curr_input.q(5, :) < 0) + 2 * pi;
        
        % Read current labeling file
        [n, t, r] = xlsread(curr_labeling_file_path);
        
        % Find first row of labeling by searching for the keyword 'Set' in
        % the second column of the file
        row1 = find(contains(t(:, column_set), 'Set'));
        
        % Find the indices of the set rows
        set_rows = find(cellfun(@(d) ~isnan(d), r(row1+1:end, column_set)));
        
        % Create a Sets variable to store the times
        Sets = cell(1, length(set_rows));
        
        % For each set
        for kk = 1 : length(set_rows)
            
            ll = 0;
            % Look for reps while we have enough rows, and while there are
            % some
            while (row1 + set_rows(kk) + ll <= size(r, 1)) && ~isnan(r{row1 + set_rows(kk) + ll, 1})
                
                % Get the path to saving the output in separated format and create
                % corresponding directory
                curr_file_savename_b = [curr_file_save_path_b, '/', 'Set', num2str(kk, '%02d'), '_', 'Rep', num2str(ll+1, '%02d'), '.mat'];
                
                % Current row
                crow = row1 + set_rows(kk) + ll;
                
                % Get all the times
                times = cell2mat(r(row1 + set_rows(kk) + ll, column_t_lift:end));
                
                % Store them in Sets variable
                Sets{kk}(ll+1, :) = times;
                
                % Also cut up the current traj and save to path b
                trial.q = curr_input.q(:, times(1):times(end));
                trial.events = times - times(1) + 1;
                trial.modelParam.Lt = curr_input.modelParam.Lt(:, times(1):times(end));
                trial.modelParam.L = curr_input.modelParam.L;
                Msetnames = fieldnames(curr_input.Markers);
                for msn = 1 : length(Msetnames)
                    Mnames = fieldnames(curr_input.Markers.(Msetnames{msn}));
                    for mn = 1 : length(Mnames)
                        trial.Markers.(Msetnames{msn}).(Mnames{mn}) = curr_input.Markers.(Msetnames{msn}).(Mnames{mn})(times(1):times(end), :);
                    end
                end
                Fnames = fieldnames(curr_input.Forceplate);
                for fn = 1 : length(Fnames)
                    trial.Forceplate.(Fnames{fn}) = curr_input.Forceplate.(Fnames{fn})(times(1):times(end), :);
                end
                trial.subject_name = subdir_name;
                trial.session_name = session_name;
                trial.set_number = kk;
                trial.rep_number = ll+1;
                
                % Save in path b
                save(curr_file_savename_b, 'trial');
                
                % Get to the next row
                ll = ll + 1;
            end            
        end
        
        % Save the whole set inside the path a
        lift_set.q = curr_input.q;
        lift_set.Markers = curr_input.Markers;
        lift_set.modelParam = curr_input.modelParam;
        lift_set.Forceplate = curr_input.Forceplate;
        lift_set.segmentation = Sets;
        save(curr_file_save_path_a, 'lift_set');
    end
    %%  </Process labeling files>
end