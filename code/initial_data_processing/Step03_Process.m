%STEP03_PROCESS script loads the processed data that has been grouped,
%interpolated, cleaned, and undergone Inverse Kinematics, it also loads the
%labeled segmentation given inside the raw data folder, within files whose 
%name ends with _UpsDownsSamples.txt, and just segments the data.
%Data is taken from:
%'../../processed_data/step02_extract_angles'
%The segmentation is taken from:
%'../../raw_data/subjects_data'
%And the results are saved twofold, in:
%   - '../../processed_data/step03a_united_segmentation' where the 
%   information from the text files is loaded and saved as a cell array
%   alongised all the raw data. Nothing is thrown away.
%   - '../../processed_data/step03b_separated_segmentation' where each
%   repetition is saved in a separate .mat file.
clear all;
close all;
clc;

%% Add model functions
addpath('../model');
addpath('../other');

%% Define directories that are in play

raw_data_dir = '../../raw_data/subjects_data';
input_data_dir = '../../processed_data/step02_extract_angles';
processed_data_dir_a = '../../processed_data/step03a_united_segmentation';
processed_data_dir_b = '../../processed_data/step03b_separated_segmentation';
if ~exist(processed_data_dir_a, 'dir'); mkdir(processed_data_dir_a); end
if ~exist(processed_data_dir_b, 'dir'); mkdir(processed_data_dir_b); end

%% Check raw data directory

% Get all within raw_data folder (by default two directories with name '.'
% and '..', so we'll start checking items with index 3)
subject_data = dir(raw_data_dir);

% Total number of items within it
num_items = length(subject_data);

% Go from the first non-default element at index 3
for ii = 3 : num_items
    
    % Get subdirectory name
    subdir_name = subject_data(ii).name;
    
    % Get the same directory name just from input data
    inputdir = [input_data_dir, '/', subdir_name];
    
    % Make the same subdirectory within the processed data directory
    savedir_a = [processed_data_dir_a, '/', subdir_name];
    savedir_b = [processed_data_dir_b, '/', subdir_name];
    if ~exist(savedir_a, 'dir'); mkdir(savedir_a); end
    if ~exist(savedir_b, 'dir'); mkdir(savedir_b); end
    
    % Get fullpath to subdirectory
    subdir_path = [raw_data_dir, '/', subdir_name];
    
    % Get all items within
    subdir_items = dir(subdir_path);
    
    % Go through subdirectory items
    text_item_names = {};
    for jj = 1 : length(subdir_items)
        % Treat only those that end with .txt
        if endsWith(subdir_items(jj).name, '.txt', 'IgnoreCase', true)
            % Save their names
            text_item_names{end+1} = subdir_items(jj).name;
        end
    end
    
    %%  <Process text files>
    % Go through all files
    for jj = 1 : length(text_item_names)
        % Get the current path to text file
        curr_text_file_path = [subdir_path, '/', text_item_names{jj}];
        % Get the current path to input file ( same name as text file but
        % without _UpsDownsSamples.txt and with .mat instead, in the input
        % directory)
        curr_input_file_path = [inputdir, '/', erase(text_item_names{jj}, '_UpsDownsSamples.txt'), '.mat'];
        % Get the path to saving the output in united format
        curr_file_save_path_a = [savedir_a, '/', erase(text_item_names{jj}, '_UpsDownsSamples.txt'), '.mat'];
        % Get the path to saving the output in separated format and create
        % corresponding directory
        curr_file_save_path_b = [savedir_b, '/', erase(text_item_names{jj}, '_UpsDownsSamples.txt')];
        if ~exist(curr_file_save_path_b); mkdir(curr_file_save_path_b); end
        
        % Read current text file
        txtfile = fileread(curr_text_file_path);
        
        % Extract parts of input string for different sets
        StringSetIndices = [];    % Initialize indices of sets within string
        kk = 1; % Initialize counter
        while ~isequal(strfind(txtfile, ['Set ', num2str(kk, '%d')]), [])   % If current set exists
            % Get its index
            StringSetIndices(kk) = strfind(txtfile, ['Set ', num2str(kk, '%d')]);
            % Augment counter
            kk = kk + 1;
        end
        % Extract the number of sets
        numsets = kk - 1;
        % Initialize cell for strings corresponding to sets
        StringSets = cell(1, numsets);
        % Prepare a data structure for sets themselves
        Sets = cell(1, numsets);
                
        % For each set extract it from the string
        for kk = 1 : numsets
            % For anything but last set
            if kk < numsets
                % Extract string corresponding to that set
                StringSets{kk} = txtfile(StringSetIndices(kk):StringSetIndices(kk+1)-1);
            % For last
            else
                % Extract string corresponding to that set
                StringSets{kk} = txtfile(StringSetIndices(kk):end);
            end
            
            % Create a structure for saving the data
            RepIndices = zeros(3, 0);
            % Find newlines in the sets
            newl = strfind(StringSets{kk}, newline);           
            
            % For each new line except last
            for ll = 1 : length(newl)
                  % Extract the repetition indices
                  if ll ~= length(newl)
                      % For all-except-last lines
                      ri = sscanf(StringSets{kk}(newl(ll) : newl(ll+1)-1), '%d --> %d --> %d');
                  else
                      % For the last line, extract the three indices
                      ri = sscanf(StringSets{kk}(newl(end):end), '%d --> %d --> %d');
                  end
                  % If we didn't extract a void line
                  if ~isequal(ri, [])
                      % Save it inside data structure 
                      % Add 1 because it was labeled in mocap
                      RepIndices(:, end+1) = ri + 1;
                  end
            end
            
            % Save the rep indices
            Sets{kk} = RepIndices;
        end
        
        
        % Load the input
        load(curr_input_file_path)
        % Save the segmentation first where all segments are still in the 
        % same file
        save(curr_file_save_path_a, 'q', 'Markers', 'Forceplate', 'modelParam', 'Sets');
        
        % Bring each sample to the range [-pi, pi] except 5th joint which
        % stays in [0, 2*pi]
        q = mod(q, 2*pi);
        q(q > pi) = q(q > pi) - 2 * pi;
        q(5, q(5, :) < 0) = q(5, q(5, :) < 0) + 2 * pi;
        
%         % Filter q
%         f_sampling = 100;
%         f_cutoff = 5;
%         order = 5;
%         q = lowpass_filter(q, f_sampling, f_cutoff, order);
        % Some spikes appear so do median filtering
        q = medfilt1(q', 3)';
        
        % Segmentation where all all sets and reps are separated
        % Go through sets
        for kk = 1 : numsets
            % Go through reps
            for ll = 1 : size(Sets{kk}, 2)
                % Extract angles of that rep
                qr_lift = q(:, Sets{kk}(1, ll) :  Sets{kk}(2, ll));
                qr_lower = q(:, Sets{kk}(2, ll) :  Sets{kk}(3, ll));
                
                % Extract markersets
                % Get the markerset names
                Msetnames = fieldnames(Markers);
                % For each markerset
                for n_mset = 1 : length(Msetnames)
                    % Get names of all markers within it
                    Mnames = fieldnames(Markers.(Msetnames{n_mset}));
                    % For each marker
                    for n_mname = 1 : length(Mnames)
                        % Extract markers for that rep
                        Markersr_lift.(Msetnames{n_mset}).(Mnames{n_mname}) = Markers.(Msetnames{n_mset}).(Mnames{n_mname})(Sets{kk}(1, ll) :  Sets{kk}(2, ll), :);
                        Markersr_lower.(Msetnames{n_mset}).(Mnames{n_mname}) = Markers.(Msetnames{n_mset}).(Mnames{n_mname})(Sets{kk}(2, ll) :  Sets{kk}(3, ll), :);
                    end
                end
                
                % Extract forces
                % Get Forceplate field names
                Fnames = fieldnames(Forceplate);
                % For each field
                for n_fp = 1 : length(Fnames)
                    % Extract the forceplate data
                    Forceplater_lift.(Fnames{n_fp}) = Forceplate.(Fnames{n_fp})(Sets{kk}(1, ll) :  Sets{kk}(2, ll), :);
                    Forceplater_lower.(Fnames{n_fp}) = Forceplate.(Fnames{n_fp})(Sets{kk}(2, ll) :  Sets{kk}(3, ll), :);
                end
                
                % Extract model parameters
                modelParamr_lift.Lt = modelParam.Lt(:, Sets{kk}(1, ll) :  Sets{kk}(2, ll));
                modelParamr_lift.L = mean(modelParamr_lift.Lt, 2);
                modelParamr_lower.Lt = modelParam.Lt(:, Sets{kk}(2, ll) :  Sets{kk}(3, ll));
                modelParamr_lower.L = mean(modelParamr_lower.Lt, 2);

                % The path to where the current rep will be saved
                curr_rep_save_path_lift = [curr_file_save_path_b, '/Set', num2str(kk, '%02d'), '_Rep', num2str(ll, '%02d'), '_lift.mat'];
                curr_rep_save_path_lower = [curr_file_save_path_b, '/Set', num2str(kk, '%02d'), '_Rep', num2str(ll, '%02d'), '_lower.mat'];
                
                % Save the current rep lifting
                save(curr_rep_save_path_lift, 'qr_lift', 'Markersr_lift', 'Forceplater_lift', 'modelParamr_lift');
                % Save the current rep lowering
                save(curr_rep_save_path_lower, 'qr_lower', 'Markersr_lower', 'Forceplater_lower', 'modelParamr_lower');
            end
        end
        
    end
    %%  </Process text files>
end