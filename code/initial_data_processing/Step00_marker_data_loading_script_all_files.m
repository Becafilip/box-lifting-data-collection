%MARKER_DATA_LOADING_SCRIPT_ALL_FILES is a script that transforms all
%the csv files within the input directory (to set within script, e.g.
%subjects_data) that DO NOT contain the keyword 'forceplate' to .mat files.
clear all;
close all;
clc;

%% Directories

% Keyword for non-forceplate data
keyword = 'forceplate';

% Input directory 
input_directory = '../../raw_data/subjects_data';

% Get subdirectories
input_subdirectories = dir(input_directory);

% Keep only the files which are directories
input_subdirectories = input_subdirectories([input_subdirectories.isdir]);

% Remove the default '.' and '..' directories
input_subdirectories = input_subdirectories(3:end);

% For each subdirectory
for iisub = 1 : length(input_subdirectories)
    
    % Current subdirectory name
    curr_subdir = [input_directory, '/', input_subdirectories(iisub).name];
    
    % Current subdirectory files
    curr_files = dir(curr_subdir);
    
    % Indices to keep
    ind2keep = [];
    
    % For each current file
    for ii = 1 : length(curr_files)
        % One who's name contains .csv and NOT THE KEYWORD
        if ~isempty(strfind(curr_files(ii).name, '.csv'))
            if isempty(strfind(curr_files(ii).name, keyword))
                % Store its index
                ind2keep = [ind2keep, ii];
            end
        end
    end
    
    % Extract files which fit the criteria
    curr_files = curr_files(ind2keep);

    % For each file
    for numfilname = 1 : length(curr_files)

        % Input filename
        input_filename = [curr_subdir, '/', curr_files(numfilname).name];

        % Read the whole CSV, get the numeric, text and raw data
        [n,t,r] = xlsread(input_filename);

        % Get the row numbers where the numeric data starts
        numDataIndex = find(n(:, 1) == 0, 1, 'first');
        n = n(numDataIndex:end, :);

        % Remember in which row the marker names are stored
        markerNameRowNumber = 4;

        % Get the row
        markerNameRow = t(markerNameRowNumber, :);

        % Initialize the Markers structure
        Markers = [];

        % Go through the row and locate unique prefixes (MarkerSet names)
        for ii = 1 : length(markerNameRow)

            % Skip empty columns
            if isempty(markerNameRow{ii});  continue; end

            % Skip unlabeled columns
            if ~isempty(strfind(markerNameRow{ii}, 'Unlabeled')); continue; end

            % For non-empty columns get the first part of the string, before the
            % semicolon so you can get the OptiTrack Motive MarkerSet names
            semicolIndex = strfind(markerNameRow{ii}, ':');
            MSName = markerNameRow{ii}(1:semicolIndex-1);

            % Get also the specific markers' name
            MName = markerNameRow{ii}(semicolIndex+1:end);

            % If the MarkerSet field already exists
            if isfield(Markers, MSName)

                % If the current marker field also already exists
                if isfield(Markers.(MSName), MName)

                    % Append the current column's data to existing
                    currCol = n(:, ii);
                    Markers.(MSName).(MName) = cat(2, Markers.(MSName).(MName), currCol);

                % If the current marker field doesn't already exist
                else

                    % Set the current column's data to be that markers value
                    currCol = n(:, ii);
                    Markers.(MSName).(MName) = currCol;

                end

            % If the MarkerSet field doesn't already exist
            else

                % Set the current MarkerSet field to contain the current Marker
                % field, and the current Marker field to contain the current column
                % as data
                currCol = n(:, ii);
                Markers.(MSName).(MName) = currCol;
            end
        end

        % Save output

        % Generate output filename by replacing .csv by .mat
        output_filename = strrep(input_filename, '.csv', '.mat');

        % Save the output data
        save(output_filename, 'Markers');
    end
end
