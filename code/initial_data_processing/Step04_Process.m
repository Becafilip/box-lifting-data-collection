%STEP04_PROCESS loads the segmented trajectories from the input directory:
%'../../processed_data/step03b_v2_separated_segmentation'
%which are grouped into 'normal' or 'squat' folders corresponding to the
%normal and squat lifting. This script collects the individual trajectories
%into a structure array named all_trials that has multiple fields:
%   - q : Joint angles
%   - events : indices of different events in lifting motion
%   - modelParam : the segment lengths, masses, COMs, intertias, total
%   height and weight
%   - Markers : Contains all Markers from the data collection
%   - Forceplate : Contains all forces, moments and cop's measured by the
%   forceplate
%   - subject_name : 'subject_' + name
%   - session_name : the name of the session given by data collector
%   - set_number : the trials are grouped into sets, this is the number of
%   the set
%   - rep_number : the trials are grouped into sets, this is the number of
%   the trial within the set
%
%Filters the trials using a median filter to remove spike noise from them
%if a flag is set.
%Saves the all_trials structure, along with some statistics on the timing 
%of the events across the dataset, but also on the normalized joint angle 
%trajectories, within the directory:
%'../../processed_data/step04_all_trials'

clear all; 
close all;
clc;

%% Add model functions
addpath('../model');
addpath('../other');
addpath('../animation_functions');

%% Define directories that are in play

input_data_dir = '../../processed_data/step03b_v2_separated_segmentation';
save_trials_dir = '../../processed_data/step04_all_trials';
load_path_BSIP = '../../processed_data/subject_dynamical_parameters';
if ~exist(save_trials_dir, 'dir'); mkdir(save_trials_dir); end

% Flags
save_trials = true;
filter_trials = true;

%% Define the keyword of the subdirectories you want to analyze
keyword_dir = 'normal';
% keyword_dir = 'squat';

%% Check input data directory

% Get all within input_data folder (by default two directories with name '.'
% and '..', so we'll start checking items with index 3)
subject_data = dir(input_data_dir);

% Total number of items within it
num_items = length(subject_data);

% Define structure of all trials
all_trials = struct([]);

% Go from the first non-default element at index 3
for ii = 3 : num_items
    
    % Get subdirectory name
    subdir_name = subject_data(ii).name;
    
    % Get the ssubdirectory path
    subdir_path = [input_data_dir, '/', subdir_name];
    
    % Get subsbudirectories
    subsubdirs = dir(subdir_path);
    
    % Total number of subitems
    num_sub = length(subsubdirs);
    
    % Go from the first non-default element at index 3
    for jj = 3 : num_sub
        
        % Get subsubdirectory name
        subsubdir_name = subsubdirs(jj).name;
        
        % If the subdirectory doesn't contain the keyword skip it
        if isequal(strfind(subsubdir_name, keyword_dir), [])
            continue
        end
        
        
        % Get the subsubdirectory path
        subsubdir_path = [subdir_path, '/', subsubdir_name];
        
        % If it does, load file by file
        subsubdir_files = dir(subsubdir_path);
        
        % Number of subsubdirectory files
        num_subsub = length(subsubdir_files);
        
        % Go file by file
        for kk = 3 : num_subsub
            
            % Get the filename
            subsubdir_filename = subsubdir_files(kk).name;
            
            % If the file is a .mat file
            if endsWith(subsubdir_filename, '.mat')
                
                % Get the file path
                subsubdir_filepath = [subsubdir_path, '/', subsubdir_filename];
                
                % Load the file
                load(subsubdir_filepath);
                
                % Store the info
                if isempty(all_trials)
                    all_trials = trial;
                else
                    all_trials(end+1) = trial;
                end
            end
        end
    end
end

%% Treat anomalies

diffmed = [];   % Vector of difference from median
% Plot all trajectories and calculate their difference from median
joint_names = {'Ankle', 'Knee', 'Hip', 'Back', 'Shoulder', 'Elbow'};
figrows = 3;
figcols = 2;
figure;
sgtitle('All trajectories')
for cc = 1 : length(all_trials)
    % Plot the joint trajectories
    for ii = 1 : figrows
       for jj = 1 : figcols
           curr = jj + (ii-1) * figcols;
           subplot(figrows, figcols, curr)
           hold on;
           tcurr = linspace(0, 1, size(all_trials(cc).q, 2));
%            plot(tcurr, medfilt1(all_trials(cc).q(curr, :), 5));
           plot(tcurr, all_trials(cc).q(curr, :));
           xlabel('t [Norm.]');
           ylabel(['\theta_{', joint_names{curr}, '}(t)']);
           title([joint_names{curr}, ' trajectory']);
       end
    end    
    % Calculate the mean square difference across joints from their median
    % trajectory
    diffmed = [diffmed, sum(mean((all_trials(cc).q - medfilt1(all_trials(cc).q, 5, [], 2)).^2, 2))];
end

% Indices of the noisy trajectories, and their plot
ii_noisy = find(diffmed > mean(diffmed));
figure;
sgtitle('Mean square deviation of a trajectory w.r.t. its medfiltered version')
hold on;
plot(diffmed', 'DisplayName', 'd = ((q-q_{med})^2)');
plot(ii_noisy, diffmed(ii_noisy)', 'ro', 'DisplayName', 'd > mean(d)');
legend;
xlabel('Trials');
ylabel('Square diff');

figure;
sgtitle('Trajectories deemed noisy according to their deviation from their medfiltered version');
% Go through the noisy trajectories
for cc = ii_noisy
    % Calculate their differennce with their medfiltered version
    d = all_trials(cc).q - medfilt1(all_trials(cc).q, 5, [], 2);
    % Plot the noisy trajectories
    for ii = 1 : figrows
       for jj = 1 : figcols
           curr = jj + (ii-1) * figcols;
           subplot(3, 2, curr)
           hold on;
           tcurr = linspace(0, 1, size(all_trials(cc).q, 2));
           plot(tcurr, all_trials(cc).q(curr, :));
%            plot(tcurr, d(curr, :));
           xlabel('t [Norm.]');
           ylabel(['\theta_{', joint_names{curr}, '}(t)']);
           title([joint_names{curr}, ' trajectory']);
       end
    end
    
    % Filter those trajectories if flag is set
    if filter_trials
        all_trials(cc).q = medfilt1(all_trials(cc).q, 5, [], 2);
    end
end

%% Process time duration

% Define the sampling time
Ts = 0.01;
% Event names
event_names = {'t_{lift}', 't_1', 't_2', 't_3', 't_4', 't_{lift, end}', 't_{lower}', 't_5', 't_6', 't_7', 't_8', 't_{lower, end}'};

% Prealocate vector of events
t_events = zeros(size(all_trials, 2), size(all_trials(1).events, 2));
% Extract event times and statistics
for ii = 1 : size(all_trials, 2)
    t_events(ii, :) = all_trials(ii).events * Ts;
end
mean_t_events = mean(t_events);
std_t_events = std(t_events);
medi_t_events = median(t_events);
min_t_events = min(t_events(:));
max_t_events = max(t_events(:));
% Number of samples we'll use for all the trajectories when resampling
Ns = round(medi_t_events(end) / Ts);
t_norm = linspace(0, 1, Ns);
% Get normalized trajectories
all_trajectories = zeros(size(all_trials(1).q, 1), Ns, length(all_trials));

% Resample all trajectories with Ns samples using spline_resample
% Plot resampled trajectories
figure;
sgtitle('Resampled trajectories');
for cc = 1 : length(all_trials)
   all_trajectories(:, :, cc) = spline_resample(all_trials(cc).q, Ns);
   for ii = 1 : figrows
       for jj = 1 : figcols
           curr = jj + (ii-1) * figcols;
           subplot(figrows, figcols, curr)
           hold on;
           plot(t_norm, squeeze(all_trajectories(curr, :, cc)));
           xlabel('t [Norm.]');
           ylabel(['\theta_{', joint_names{curr}, '}(t)']);
           title([joint_names{curr}, ' trajectory']);
       end
   end
end

% Calculate the mean and standard deviation of the trajectories accross
% trials
mean_trajectory = mean(all_trajectories, 3);
std_trajectory = std(all_trajectories, [], 3);


%% Save the trajectories and statistics

% Save statistics
all_trials_statistics.Ts = Ts;
all_trials_statistics.event_names = event_names;
all_trials_statistics.mean_t_events = mean_t_events;
all_trials_statistics.std_t_events = std_t_events;
all_trials_statistics.medi_t_events = medi_t_events;
all_trials_statistics.min_t_events = min_t_events;
all_trials_statistics.max_t_events = max_t_events;

all_trials_statistics.Ns = Ns;
all_trials_statistics.mean_trajectory = mean_trajectory;
all_trials_statistics.std_trajectory = std_trajectory;
all_trials_statistics.all_trajectories = all_trajectories;

% Add the modelParam to the trials
for ii = 1 : length(all_trials)
    
    % Load the subject's BSIP parameters, they are stored inside the
    % modelParam structure
    curr_load_path = [load_path_BSIP, '/', all_trials(ii).subject_name, '/', all_trials(ii).subject_name, '.mat'];
    load(curr_load_path);
    
    % Copy over fields that we want
    for fn = fieldnames(modelParam)'
        all_trials(ii).modelParam.(fn{1}) = modelParam.(fn{1});
    end
    
    % Segment lengths, masses, and COMs from anthropometric table
    AT_L = [];
    M = [];
    COM = [];
    for ll = 1 : 6   
        AT_L = eval(['[AT_L, all_trials(ii).modelParam.L', num2str(ll), '];']);
        M = eval(['[M, all_trials(ii).modelParam.M', num2str(ll), '];']);
        COM = eval(['[COM, all_trials(ii).modelParam.COM', num2str(ll), '];']);
    end
    
    % Add them as a matrix parameter
    all_trials(ii).modelParam.AT_L = AT_L';
    all_trials(ii).modelParam.M = M';
    all_trials(ii).modelParam.COM = COM;
    
    % Set the measured parameters to be the L1,L2,...,L6
    for ll = 1 : 6
        eval(['all_trials(ii).modelParam.L', num2str(ll), ' = all_trials(ii).modelParam.L(', num2str(ll), ');']);
    end
end

% Get the saved path
save_path = [save_trials_dir, '/', keyword_dir, '_trials.mat'];
% Save if flag is set
if save_trials; save(save_path, 'all_trials', 'all_trials_statistics'); end