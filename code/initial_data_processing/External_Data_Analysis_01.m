%DATA_ANALYSIS01 loads the segmented trajectories from the input directory:
%'../../processed_data/step03b_v2_separated_segmentation'
%which are grouped into 'normal' or 'squat' folders.
clear all; 
close all;
clc;

%% Add model functions
addpath('../model');
addpath('../other');
addpath('../animation_functions/');

%% Define directories that are in play

input_data_dir = '../../processed_data/step03b_v2_separated_segmentation';
processed_data_dir = '../../processed_data/step04_v2_grouped';
% save_graphs_dir_1 = '../../processed_data/graphs/Analysis01_v2';
% if ~exist(processed_data_dir, 'dir'); mkdir(processed_data_dir); end
% if ~exist(save_graphs_dir_1, 'dir'); mkdir(save_graphs_dir_1); end

%% Define the keyword of the subdirectories you want to analyze
keyword_dir = 'normal';
% keyword_dir = 'squat';

%% Check input data directory

% Get all within input_data folder (by default two directories with name '.'
% and '..', so we'll start checking items with index 3)
subject_data = dir(input_data_dir);

% Total number of items within it
num_items = length(subject_data);

% Define structure of all trials
all_trials = struct([]);

% Go from the first non-default element at index 3
for ii = 3 : num_items
    
    % Get subdirectory name
    subdir_name = subject_data(ii).name;
    
    % Get the ssubdirectory path
    subdir_path = [input_data_dir, '/', subdir_name];
    
    % Get subsbudirectories
    subsubdirs = dir(subdir_path);
    
    % Total number of subitems
    num_sub = length(subsubdirs);
    
    % Go from the first non-default element at index 3
    for jj = 3 : num_sub
        
        % Get subsubdirectory name
        subsubdir_name = subsubdirs(jj).name;
        
        % If the subdirectory doesn't contain the keyword skip it
        if isequal(strfind(subsubdir_name, keyword_dir), [])
            continue
        end
        
        
        % Get the subsubdirectory path
        subsubdir_path = [subdir_path, '/', subsubdir_name];
        
        % If it does, load file by file
        subsubdir_files = dir(subsubdir_path);
        
        % Number of subsubdirectory files
        num_subsub = length(subsubdir_files);
        
        % Go file by file
        for kk = 3 : num_subsub
            
            % Get the filename
            subsubdir_filename = subsubdir_files(kk).name;
            
            % If the file is a .mat file
            if endsWith(subsubdir_filename, '.mat')
                
                % Get the file path
                subsubdir_filepath = [subsubdir_path, '/', subsubdir_filename];
                
                % Load the file
                load(subsubdir_filepath);
                
                % Store the info
                if isempty(all_trials)
                    all_trials = trial;
                else
                    all_trials(end+1) = trial;
                end
            end
        end
    end
end

load ../../external_files/opt_res.mat
load ../../external_files/opt_res_accel.mat
load ../../external_files/opt_res_jerk.mat
load ../../external_files/opt_res_power.mat
load ../../external_files/opt_res_eevel.mat
load ../../external_files/opt_res_eeaccel.mat
load ../../external_files/opt_res_comvel.mat
load ../../external_files/opt_res_comacc.mat

%% Cut all events at box grasp
for ii = 1 : size(all_trials, 2)
    all_trials(ii).q = all_trials(ii).q(:, 1:all_trials(ii).events(3));
    all_trials(ii).events = all_trials(ii).events(1:3);
end
%% Process time duration
% Define the sampling time
Ts = 0.01;
% Event names
event_names = {'t_{lift}', 't_1', 't_2', 't_3', 't_4', 't_{lift, end}', 't_{lower}', 't_5', 't_6', 't_7', 't_8', 't_{lower, end}'};
% Number of rows and columns in plot
figrows = 5;
figcols = 3;

% Prealocate vector of events
t_events = zeros(size(all_trials, 2), size(all_trials(1).events, 2));

% Extract event times and statistics
for ii = 1 : size(all_trials, 2)
    t_events(ii, :) = all_trials(ii).events * Ts;
end
mean_t_events = mean(t_events);
std_t_events = std(t_events);
medi_t_events = median(t_events);
min_t_events = min(t_events(:));
max_t_events = max(t_events(:));

%% % With normalized times

% Number of rows and columns of the plot
figrows = 5;
figcols = 3;
% Names of the events occuring
event_names = {'t^n_{lift}', 't^n_1', 't^n_2', 't^n_3', 't^n_4', 't^n_{lift, end}', 't^n_{lower}', 't^n_5', 't^n_6', 't^n_7', 't^n_8', 't^n_{lower, end}'};
% Plot


%% Process the trajectories


% Number of samples we'll use for all the trajectories when resampling
Ns = round(medi_t_events(end) / Ts);
% Title of the figure
figtitle = {['Mean trajectories and standard deviations for normal lifting-lowering']; ['motion cycle, with sample size of ', num2str(size(all_trials, 2), '%d'), ' trials']};
% Number of standard deviations to be plotted
numstddev = 3;
% Plot
mean_q = plot_normalized_trajectories(all_trials, Ns, figtitle, numstddev);
% Number of rows and columns of the figure

q_star_r = spline_resample(q_star, Ns);
q_star_accel_r = spline_resample(q_star_accel, Ns);
q_star_jerk_r = spline_resample(q_star_jerk, Ns);
q_star_power_r = spline_resample(q_star_power, Ns);
q_star_eevel_r = spline_resample(q_star_eevel, Ns);
q_star_eeaccel_r = spline_resample(q_star_eeaccel, Ns);
q_star_comvel_r = spline_resample(q_star_comvel, Ns);
q_star_comacc_r = spline_resample(q_star_comacc, Ns);

q_star_r(5, :) = mod(q_star_r(5, :), 2*pi);
q_star_accel_r(5, :) = mod(q_star_accel_r(5, :), 2*pi);
q_star_jerk_r(5, :) = mod(q_star_jerk_r(5, :), 2*pi);
q_star_power_r(5, :) = mod(q_star_power_r(5, :), 2*pi);
q_star_eevel_r(5, :) = mod(q_star_eevel_r(5, :), 2*pi);
q_star_eeaccel_r(5, :) = mod(q_star_eeaccel_r(5, :), 2*pi);
q_star_comvel_r(5, :) = mod(q_star_comvel_r(5, :), 2*pi);
q_star_comacc_r(5, :) = mod(q_star_comacc_r(5, :), 2*pi);
curve_col = jet(8);

t_norm = linspace(0, 1, Ns);
figcols = 2;
figrows = 3;
Joints = {'Ankle', 'Knee', 'Hip', 'Back', 'Shoulder', 'Elbow'};
for ii = 1 : figrows
    for jj = 1 : figcols        
        % Current joint/plot
        curr = jj + (ii-1)*figcols;
        
        % Create subplot grid in row major order
        subplot(figrows, figcols, curr)
        hold on;
        
        % Plot mean joint profile as well as std
        x = t_norm;
        plot(x, q_star_r(curr, :), 'Color', curve_col(1, :), 'LineWidth', 1, 'DisplayName', ['Torque']);
        plot(x, q_star_accel_r(curr, :), 'Color', curve_col(2, :), 'LineWidth', 1, 'DisplayName', ['Accel']);
        plot(x, q_star_jerk_r(curr, :), 'Color', curve_col(3, :), 'LineWidth', 1, 'DisplayName', ['Jerk']);
        plot(x, q_star_power_r(curr, :), 'Color', curve_col(4, :), 'LineWidth', 1, 'DisplayName', ['Power']);
        plot(x, q_star_eevel_r(curr, :), 'Color', curve_col(5, :), 'LineWidth', 1, 'DisplayName', ['EEVel']);
        plot(x, q_star_eeaccel_r(curr, :), 'Color', curve_col(6, :), 'LineWidth', 1, 'DisplayName', ['EEAcc']);
        plot(x, q_star_comvel_r(curr, :), 'Color', curve_col(7, :), 'LineWidth', 1, 'DisplayName', ['COMVel']);
        plot(x, q_star_comacc_r(curr, :), 'Color', curve_col(8, :), 'LineWidth', 1, 'DisplayName', ['COMAcc']);
    end
end

% Name of the saved plot

set(gcf, 'Position', get(0, 'Screensize'));

%%
Tsh = Ts;
Ts = 0.01;
figure;
options.legend_entry1 = 'Mean';
options.legend_entry2 = 'Torque';
options.show_legend = true;
Animate_Two_nDOF(mean_q, all_trials(1).modelParam.L, q_star_r, all_trials(1).modelParam.L, Ts, options)
%saveas(gcf, plot_path);

% %% Analyze trajectories per subject
% 
% % Number of samples we'll use for all the trajectories when resampling
% Ns = round(medi_t_events(end) / Ts);
% 
% % Extract useful info in adequate data structures
% names = arrayfun(@(elem) string(elem.subject_name), all_trials);
% set_numbers = arrayfun(@(elem) elem.set_number, all_trials);
% rep_numbers = arrayfun(@(elem) elem.rep_number, all_trials);
% 
% % Get unique subjects
% [subjects_unique, ~, subjects_indices] = unique(names);
% 
% % For each subject
% for ii = 1 : length(subjects_unique)
%     % Current subject
%     curr_subject = strrep(subjects_unique(ii), 'subject_', '');
%     curr_subject = upper(curr_subject);
%     % Get current subject's trials
%     curr_trials = all_trials(subjects_indices == ii);
%     % Make a title for current subjects trials
%     curr_figtitle = {['Mean and standard deviations for joint']; ['trajectories of the lift/lower cycle']; ['for subject ', char(curr_subject)]};
%     % Number of standard deviations
%     curr_numstddev = 3;
% %     Plot
%     plot_events(curr_trials, Ts, event_names, figrows, figcols);
%     set(gcf, 'Position', get(0, 'Screensize'));
%     plot_normalized_events(curr_trials, event_names, figrows, figcols);
%     set(gcf, 'Position', get(0, 'Screensize'));
%     plot_normalized_trajectories(curr_trials, Ns, curr_figtitle, curr_numstddev);
%     set(gcf, 'Position', get(0, 'Screensize'));
% end
% 
% 
% %% For each subject's set
% % For each subject
% for ii = 1 : length(subjects_unique)
%     
%     % Current subject
%     curr_subject = subjects_unique(ii);
%     % Subject name
%     curr_subject_name = upper(strrep(curr_subject, 'subject_', ''));
%     % Find the sets for current subject
%     [sets_unique, ~, sets_indices] = unique(set_numbers(subjects_indices));
%     
%     % For each set
%     for jj = 1 : length(sets_unique)
%         % Get current subject's trials from current set
%         curr_trials = all_trials(subjects_indices' == ii & set_numbers == sets_unique(jj));
%         % Make a title for current subjects trials
%         curr_figtitle = {['Mean and standard deviations for joint']; ['trajectories of the lift/lower cycle']; ['for subject ', char(curr_subject_name), ' Set ', num2str(sets_unique(jj), '%d')]};
%         % Number of standard deviations
%         curr_numstddev = 3;
%         % Plot
% %         plot_normalized_trajectories(curr_trials, Ns, curr_figtitle, curr_numstddev);
%     end
% end
