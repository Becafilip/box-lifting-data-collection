%STEP02_PROCESS extracts joint angles from the already processed data
%inside the processed data directory:
%'../../processed_data/step01_fill_and_group'
%modifies it and saves the modifications in:
%'../../processed_data/step02_extract_angles'
%It:
%   - Takes in the Marker and Forceplate data, extracts the segment lengths
%   and calculates the segment angles.
%   - Brings the segment angles to a [-pi, pi] range

clear all;
close all;
clc;

%% Define directories that are in play
input_data_dir = '../../processed_data/step01_fill_and_group';
processed_data_dir = '../../processed_data/step02_extract_angles';

%% Add the directory containing the FKM function
addpath('../model/');

%% Check input data directory

% Get all within raw_data folder (by default two directories with name '.'
% and '..', so we'll start checking items with index 3)
subject_data = dir(input_data_dir);

% Total number of items within it
num_items = length(subject_data);
%%
tic
% Go from the first non-default element at index 3
for ii = 3 : num_items
    
    % Get subdirectory name
    subdir_name = subject_data(ii).name;
    
    % Make the same subdirectory within the processed data directory
    savedir = [processed_data_dir, '/', subdir_name];
    if ~exist(savedir, 'dir'); mkdir(savedir); end
    
    % Get fullpath to subdirectory
    subdir_path = [input_data_dir, '/', subdir_name];
    
    % Get all items within
    subdir_items = dir(subdir_path);
    
    % Go through subdirectory items
    dotmat_item_names = {};
    for jj = 1 : length(subdir_items)
        % Treat only those that end with .mat
        if endsWith(subdir_items(jj).name, '.mat', 'IgnoreCase', true)
            % Save their names
            dotmat_item_names{end+1} = subdir_items(jj).name;
        end
    end
    
    % Go through the items which contain Markers and Forceplates
    for jj = 1 : length(dotmat_item_names)
        
        % Load the current item
        load_path = [subdir_path, '/', dotmat_item_names{jj}];
        load(load_path)
        
        % Get the number of samples
        NbSamples = size(Markers.BODY.RANK, 1);
        
        % Initialize body segment length vector
        L = zeros(6, NbSamples);
        
        % Calculate body segment lengths approx at each time
        for kk = 1 : NbSamples
            % Shank
            L(1, kk) = norm(Markers.BODY.RKNE(kk, 1:2) - Markers.BODY.RANK(kk, 1:2));
            % Thigh
            L(2, kk) = norm(Markers.BODY.RGTR(kk, 1:2) - Markers.BODY.RKNE(kk, 1:2));
            % Pelvis + Abdomen
            L(3, kk) = norm(Markers.BODY.BACK(kk, 1:2) - Markers.BODY.RGTR(kk, 1:2));
            % Thorax
            L(4, kk) = norm(Markers.BODY.RSHO(kk, 1:2) - Markers.BODY.BACK(kk, 1:2));
            % Upper Arm
            L(5, kk) = norm(Markers.BODY.RELB(kk, 1:2) - Markers.BODY.RSHO(kk, 1:2));
            % Forearm
            L(6, kk) = norm(Markers.BODY.RWRI(kk, 1:2) - Markers.BODY.RELB(kk, 1:2));
        end
        
        % Get the mean and standard deviation of segment lengths
        meanL = mean(L, 2);
        stdL = std(L, 0, 2);
        
        % Robot base
        base = Markers.BODY.RANK(:, 1:2);
        
        % Prealocate joint angles
        q = zeros(6, NbSamples);
        
        % Concatenate desired positions along 3rd tensor dimension
        P_meas = cat(3, Markers.BODY.RANK(:, 1:2), Markers.BODY.RKNE(:, 1:2), Markers.BODY.RGTR(:, 1:2),...
                        Markers.BODY.BACK(:, 1:2), Markers.BODY.RSHO(:, 1:2), Markers.BODY.RELB(:, 1:2), Markers.BODY.RWRI(:, 1:2));
                    
        % Initial guess
        q0 = zeros(6, 1);
        q0(1) = pi/2;
        
        % Options
        op=optimoptions('lsqnonlin','Display','off');
        
        % Optimize for each sample
        for ii = 1 : NbSamples
            [q(:,ii), resnorm]=lsqnonlin(@(x)Step02_angle_id_cf(x,squeeze(P_meas(ii, :, :)), base(ii, :), L), q0, [], [], op);
        end
        
        % Store model parameters inside one struct
        modelParam.Lt = L;  % Lengths as a function of time
        modelParam.L = meanL;   % "Lenghts" of segments
        
        % Save the extracted angles alongside markers and forceplates in
        % the new directory
        savename = [savedir, '/', dotmat_item_names{jj}];        
        save(savename, 'Markers', 'Forceplate', 'q', 'modelParam');
        
    end
end
toc 