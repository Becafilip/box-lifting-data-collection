function plot_normalized_events(all_trials, event_names, figrows, figcols)
%PLOT_NORMALIZED_EVENTS plots the histograms of all the timings of events 
%in the all_trials structure in a subplot grid with a given number of rows 
%and columns, and annotates them with their name in event_names.
%
%   PLOT_NORMALIZED_EVENTS(all_trials, event_names, figrows, figcols)

% Prealocate vector of events in normalized time
t_norm_events= zeros(size(all_trials, 2), size(all_trials(1).events, 2));

% Extract event normalized times and statistics
for ii = 1 : size(all_trials, 2)
    t_norm_events(ii, :) = all_trials(ii).events / all_trials(ii).events(end);
end
mean_t_norm_events = mean(t_norm_events);
std_t_norm_events = std(t_norm_events);
medi_t_norm_event = median(t_norm_events);
min_t_norm_event = min(t_norm_events(:));
max_t_norm_event = max(t_norm_events(:));

% Event names
event_names = {'t^n_{lift}', 't^n_1', 't^n_2', 't^n_3', 't^n_4', 't^n_{lift, end}', 't^n_{lower}', 't^n_5', 't^n_6', 't^n_7', 't^n_8', 't^n_{lower, end}'};

% Check if enough rows and columns to leave an empty row at the end
if (figrows-1) * figcols < size(t_norm_events, 2)
    error('too few rows and columns')
end
% Plot the histograms and the analytic gaussians in last row
figure;
sgtitle({sprintf('The histograms for different event occurence times in the lifting/lowering motion, sample size of %d', size(t_norm_events, 1)); });
for ii = 1 : figrows-1
    for jj = 1 : figcols
        % Get current index of subplot
        curr = jj + (ii - 1) * figcols;
        
        % Create subplot
        subplot(figrows, figcols, curr);
        hold on;
        h = histogram(t_norm_events(:, curr), 'NumBins', 15, 'DisplayName', [event_names{curr}]);
        plot([mean_t_norm_events(curr), mean_t_norm_events(curr)], [0 max(h.Values)], 'r', 'LineWidth', 5, 'DisplayName', 'Mean');
        plot([medi_t_norm_event(curr), medi_t_norm_event(curr)], [0 max(h.Values)], 'LineWidth', 5, 'DisplayName', 'Median');
        xlim([min_t_norm_event, max_t_norm_event]);
        xlabel('Time [Norm.]');
        ylabel({'Number of'; 'occurences'});
        title({['The time ', event_names{curr}, sprintf(', sample size of %d trials', length(all_trials))]});
        legend('Location', 'Best');
        grid;
    end
end
% Get linear time for plotting analytic gaussians
t_linear = linspace(min_t_norm_event, max_t_norm_event, 1000);
% Get current index of subplot
ii = ii + 1;
jj = 1 : figcols;
curr = jj + (ii - 1) * figcols;
% Create subplot
subplot(figrows, figcols, curr);
hold on;
curve_col = jet(size(t_norm_events, 2));
for ii = 2 : size(t_norm_events, 2)-1   % Don't show 1st and last 
    plot(t_linear, gaussian_prob(mean_t_norm_events(ii), std_t_norm_events(ii), t_linear, 1e-3, 1e-3), 'DisplayName', event_names{ii}, 'Color', curve_col(ii, :), 'LineWidth', 2);
end
grid;
legend('NumColumns', figcols);
[low, upp] = expand_range(min_t_norm_event, max_t_norm_event, 1.1);
xlim([low, upp]);
xlabel('Time [Norm.]');
ylabel({'Probability density'; 'of occurence'});
title({['Probabilistic timing of different events during the lift']});