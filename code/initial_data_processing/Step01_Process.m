%STEP01_PROCESS script loads the raw data that has been transposed from
%.csv files to .mat files, by scripts in the data directory:
%'../../raw_data/subjects_data'
%modifies them and saves them to the processed data directory:
%'../../processed_data/step01_fill_and_group'
%It:
%   - Fills gaps in Markers using the FillCubic function
%   - Rotates the Marker coordinates so that the X-axis is pointing in the
%   direction the human is looking, while the Z-axis is pointing from the
%   human's left to his right.
%   - Undersamples Forceplate data if it has more samples than the Markers
%   (as we've had some data that was recorded with 1000Hz on the Forceplate
%   and 100Hz on the Optitrack)
%   - Groups together the Markers and Forceplate structures and saves them

clear all;
close all;
clc;

%% Import some functions
addpath('other');

%% Define directories that are in play
raw_data_dir = '../../raw_data/subjects_data';
processed_data_dir = '../../processed_data/step01_fill_and_group';

%% Define the rotation matrix for the Markers
RMarkers = roty(180);

%% Check input data directory

% Get all within raw_data folder (by default two directories with name '.'
% and '..', so we'll start checking items with index 3)
subject_data = dir(raw_data_dir);

% Total number of items within it
num_items = length(subject_data);

% Go from the first non-default element at index 3
for ii = 3 : num_items
    
    % Get subdirectory name
    subdir_name = subject_data(ii).name;
    
    % Make the same subdirectory within the processed data directory
    savedir = [processed_data_dir, '/', subdir_name];
    if ~exist(savedir, 'dir'); mkdir(savedir); end
    
    % Get fullpath to subdirectory
    subdir_path = [raw_data_dir, '/', subdir_name];
    
    % Get all items within
    subdir_items = dir(subdir_path);
    
    % Go through subdirectory items
    dotmat_item_names = {};
    for jj = 1 : length(subdir_items)
        % Treat only those that end with .mat
        if endsWith(subdir_items(jj).name, '.mat', 'IgnoreCase', true)
            % Save their names
            dotmat_item_names{end+1} = subdir_items(jj).name;
        end
    end
    
    % Assume that we have Marker and Forceplate data, process the items by
    % pair
    for jj = 1 : 2 : length(dotmat_item_names) - 1

        % Marker file name
        Mfilename = dotmat_item_names{jj};
        % Forceplate file name
        Ffilename = dotmat_item_names{jj+1};
        
        % Get the full item path for both items
        Markerpath = [subdir_path, '/', Mfilename];
        Forceplatepath = [subdir_path, '/', Ffilename];
        
        % Load both files and get Markers and Forceplate structure
        load(Markerpath);
        load(Forceplatepath);
        
        % Get the markerset names
        Msetnames = fieldnames(Markers);
        
        % For each markerset
        for kk = 1 : length(Msetnames)
            
            % Get names of all markers within it
            Mnames = fieldnames(Markers.(Msetnames{kk}));
            
            % For each marker
            for ll = 1 : length(Mnames)
                
                % Fill the gaps
                Markers.(Msetnames{kk}).(Mnames{ll}) = FillGapCubic(Markers.(Msetnames{kk}).(Mnames{ll}));
                
                % Rotate the Markers' coordinate system
                Markers.(Msetnames{kk}).(Mnames{ll}) = (RMarkers * Markers.(Msetnames{kk}).(Mnames{ll}).').';
                
                % Get the number of samples
                NbSamples = size(Markers.(Msetnames{kk}).(Mnames{ll}), 1);
                
            end
        end
        
        % Get Forceplate field names
        Fnames = fieldnames(Forceplate);
        
        % For each field
        for kk = 1 : length(Fnames)
            
            % If there are more samples in Forceplate than in Markers
            if size(Forceplate.(Fnames{kk}), 1) > NbSamples
                
                % Resample the Forceplate data with following step
                resize_step = size(Forceplate.(Fnames{kk}), 1) / NbSamples;
                
                % Resample the Forceplate data
                Forceplate.(Fnames{kk}) = Forceplate.(Fnames{kk})(1:resize_step:end, :);
            end
        end
        
        % Generate the directory name of where to save the modified files
        savename = [savedir, '/', Mfilename];        
        
        % Save the modified files
        save(savename, 'Markers', 'Forceplate');
        
    end
end