function plot_events(all_trials, Ts, event_names, figrows, figcols)
%PLOT_EVENTS plots the histograms of all the timings of events in the
%all_trials structure in a subplot grid with a given number of rows and
%columns, and annotates them with their name in event_names.
%
%   PLOT_EVENTS(all_trials, Ts, event_names, figrows, figcols)


% Prealocate vector of events
t_events = zeros(size(all_trials, 2), size(all_trials(1).events, 2));

% Extract event times and statistics
for ii = 1 : size(all_trials, 2)
    t_events(ii, :) = all_trials(ii).events * Ts;
end
mean_t_events = mean(t_events);
std_t_events = std(t_events);
medi_t_events = median(t_events);
min_t_events = min(t_events(:));
max_t_events = max(t_events(:));

% Check if enough rows and columns to leave an empty row at the end
if (figrows-1) * figcols < size(t_events, 2)
    error('too few figrows and figcolumns')
end
% Plot the histograms and the analytic gaussians in last row
figure;
sgtitle({sprintf('The histograms for different event occurence times in the lifting/lowering motion, sample size of %d', size(t_events, 1)); });
for ii = 1 : figrows-1
    for jj = 1 : figcols
        % Get current index of subplot
        curr = jj + (ii - 1) * figcols;
        
        % Skip if all events have been treated
        if curr > size(t_events, 2)
            continue
        end
        
        % Create subplot
        subplot(figrows, figcols, curr);
        hold on;
        h = histogram(t_events(:, curr), 'NumBins', 15, 'DisplayName', [event_names{curr}]);
        plot([mean_t_events(curr), mean_t_events(curr)], [0 max(h.Values)], 'r', 'LineWidth', 5, 'DisplayName', 'Mean');
        plot([medi_t_events(curr), medi_t_events(curr)], [0 max(h.Values)], 'LineWidth', 5, 'DisplayName', 'Median');
        xlim([min_t_events, max_t_events]);
        xlabel('Time [s]');
        ylabel({'Number of'; 'occurences'});
        title({['The time ', event_names{curr}, sprintf(', sample size of %d trials', length(all_trials))]});
        legend('Location', 'Best');
        grid;
    end
end
% Get linear time for plotting analytic gaussians
t_linear = linspace(min_t_events, max_t_events, 1000);
% Get current index of subplot
ii = ii + 1;
jj = 1 : figcols;
curr = jj + (ii - 1) * figcols;
% Create subplot
subplot(figrows, figcols, curr);
hold on;
curve_colors = jet(size(t_events, 2));
for ii = 1 : size(t_events, 2)
    plot(t_linear, gaussian_prob(mean_t_events(ii), std_t_events(ii), t_linear), 'DisplayName', event_names{ii}, 'Color', curve_colors(ii, :), 'LineWidth', 2);
end
grid;
legend('NumColumns', figcols);
[low, upp] = expand_range(min_t_events, max_t_events, 1.1);
xlim([low, upp]);
xlabel('Time [s]');
ylabel({'Probability density'; 'of occurence'});
title({['Probabilistic timing of different events during the lift']});
end