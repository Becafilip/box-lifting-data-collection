function plot_normalized_trajectories(all_trials, Ns, figtitle, numstddev)
%PLOT_NORMALIZED_TRAJECTORIES plots the normalized trajectories contained
%in the all_trials structure array. The trajectories are resampled with Ns
%points, and are plotted on a figure with figtitle against their mean and
%numstddev times standard deviation.
%   PLOT_NORMALIZED_TRAJECTORIES(all_trials, Ns, figtitle, numstddev)

% If figtitle isnt passed, set to default
if isequal(figtitle, [])
    figtitle = {['Mean trajectories and standard deviations for normal lifting and lowering motion']};
end

% If numstddev isnt passed, set to default
if isequal(numstddev, [])
    numstddev = 3;
end
    
% Prealocate structure to store all resampled trials
all_trajectories = zeros(size(all_trials(1).q, 1), Ns, size(all_trials, 2));

% Treat the trajectories of all trials
for kk = 1 : size(all_trials, 2)
    % Resample with the number of samples Ns, using spline resampling
    all_trajectories(:, :, kk) = spline_resample(all_trials(kk).q, Ns);
end

% Get the mean
mean_trajectory = mean(all_trajectories, 3);
std_trajectory = std(all_trajectories, 0, 3);

% Get the normalized time vector
t_norm = linspace(0, 1, Ns);
% Number of rows and columns of the figure
figcols = 2;
figrows = 3;
% Joint names
Joints = {'Ankle', 'Knee', 'Hip', 'Back', 'Shoulder', 'Elbow'};
% Draw the mean +- std for the trajectories
figure;
sgtitle(figtitle);
for ii = 1 : figrows
    for jj = 1 : figcols        
        % Current joint/plot
        curr = jj + (ii-1)*figcols;
        
        % Create subplot grid in row major order
        subplot(figrows, figcols, curr)
        hold on;
        
        % Plot mean joint profile as well as std
        x = t_norm;
        y = mean_trajectory(curr,:);
        y1 = mean_trajectory(curr,:) + numstddev*std_trajectory(curr, :);
        y2 = mean_trajectory(curr,:) - numstddev*std_trajectory(curr, :);
        plot(x, y1, 'k', 'DisplayName', ['+-', num2str(numstddev, '%d'), 'std']);
        plot(x, y2, 'k', 'HandleVisibility', 'Off');
        patch([x fliplr(x)], [y1 fliplr(y2)], [220,220,220]/256, 'DisplayName', ['+-', num2str(numstddev, '%d'), 'std-region'])
        plot(x, y, 'LineWidth', 2, 'DisplayName', ['Mean']);
%         ylim([-pi, pi])
%         if strcmp(Joints{curr}, 'Shoulder'); ylim([0, 2*pi]); end
        
        % Labels
        xlabel('Time [Norm.]');
        ylabel([Joints{curr} ' angle [rad]']);
        title([Joints{curr} ' joint trajectory']);
        if curr == 2; legend('Location', 'Best'); end
        grid;
    end
end
