%DATA_ANALYSIS01_V2 loads the segmented trajectories from the input 
%directory:
%'../../processed_data/step04_all_trials'
%which are grouped into either 'normal' or 'squat' files/structures.
%This script plots some statistical info concerning those trajectories and
%saves those graphs according to a flag into the directory
%'../../processed_data/graphs/Analysis01_v2'
%   
clear all; 
close all;
clc;

%% Add model functions
addpath('../model');
addpath('../other');
addpath('../animation_functions')

%% Define directories that are in play

input_data_dir = '../../processed_data/step04_all_trials';
save_graphs_dir_1 = '../../processed_data/graphs/Analysis01_v2';
if ~exist(save_graphs_dir_1, 'dir'); mkdir(save_graphs_dir_1); end

% Flags
save_graphs = false;
plot_per_subject_graphs = false;
save_per_subject_graphs = false;
plot_per_set_graphs = false;
save_per_set_graphs = false;

%% Define the keyword of the subdirectories you want to analyze
keyword_dir = 'normal';
% keyword_dir = 'squat';

%% Check input data directory

% Load path
load_path = [input_data_dir, '/', keyword_dir, '_trials.mat'];

% Load input data
load(load_path)

%% Plot historams of the event occurence times in the lifting motion and model their distribution as gaussian
% Define the sampling time
Ts = all_trials_statistics.Ts;
% Event names
event_names = all_trials_statistics.event_names;
% Number of rows and columns in plot
figrows = 5;
figcols = 3;
% Plot
plot_events(all_trials, Ts, event_names, figrows, figcols);
% Name of the saved plot
plot_name = 'absolute_event_times_histograms.png';
plot_path = [save_graphs_dir_1, '/', plot_name];
set(gcf, 'Position', get(0, 'Screensize')); % Set to fullscreen before saving
% Save if flag is set
if save_graphs; saveas(gcf, plot_path); end


%% Plot histograms of the event occurence normalized times in the lifting motion and model their distribution as gaussian
% Plot
plot_normalized_events(all_trials, event_names, figrows, figcols);
% Name of the saved plot
plot_name = 'normalized_event_times_histograms.png';
plot_path = [save_graphs_dir_1, '/', plot_name];
set(gcf, 'Position', get(0, 'Screensize')); % Set to fullscreen before saving
% Save if flag is set
if save_graphs; saveas(gcf, plot_path); end


%% Plot the normalized trajectory means, and means plus standard deviations
% Number of samples we'll use for all the trajectories when resampling
Ns = all_trials_statistics.Ns;
% Title of the figure
figtitle = {['Mean trajectories and standard deviations for normal lifting-lowering']; ['motion cycle, with sample size of ', num2str(size(all_trials, 2), '%d'), ' trials']};
% Number of standard deviations to be plotted
numstddev = 3;
% Plot
plot_normalized_trajectories(all_trials, Ns, figtitle, numstddev);
% Name of the saved plot
plot_name = 'joint_trajectories_of_full_cycle_in_all_trials.png';
plot_path = [save_graphs_dir_1, '/', plot_name];
set(gcf, 'Position', get(0, 'Screensize')); % Set to fullscreen before saving
% Save if flag is set
if save_graphs; saveas(gcf, plot_path); end


%% Analyze trajectories per subject

% Extract useful info in adequate data structures
names = arrayfun(@(elem) string(elem.subject_name), all_trials);
set_numbers = arrayfun(@(elem) elem.set_number, all_trials);
rep_numbers = arrayfun(@(elem) elem.rep_number, all_trials);

% Get unique subjects
[subjects_unique, ~, subjects_indices] = unique(names);

% For each subject
for ii = 1 : length(subjects_unique)
    % Current subject
    curr_subject = strrep(subjects_unique(ii), 'subject_', '');
    curr_subject = upper(curr_subject);
    % Get current subject's trials
    curr_trials = all_trials(subjects_indices == ii);
    % Make a title for current subjects trials
    curr_figtitle = {['Mean and standard deviations for joint trajectories of the lift/lower cycle']; ['for subject ', char(curr_subject), ' with ', num2str(length(curr_trials)), ' trials']};
    % Number of standard deviations
    curr_numstddev = 3;
    % Plot the graphs that corncern single subjects
    if plot_per_subject_graphs
        % Create the current saving directory
        if save_per_subject_graphs
            curr_save_dir = [save_graphs_dir_1, '/', char(subjects_unique(ii))];
            if ~exist(curr_save_dir); mkdir(curr_save_dir); end
        end
        
        % Plot historams of the event occurence times in the lifting motion and model their distribution as gaussian
        plot_events(curr_trials, Ts, event_names, figrows, figcols);
        set(gcf, 'Position', get(0, 'Screensize'));
        if save_per_subject_graphs
            plot_name = 'absolute_event_times_histogram.png';
            curr_plot_path = [curr_save_dir, '/', plot_name];
            saveas(gcf, curr_plot_path);
        end
            
        % Plot historams of the event occurence normalized times in the lifting motion and model their distribution as gaussian
        plot_normalized_events(curr_trials, event_names, figrows, figcols);
        set(gcf, 'Position', get(0, 'Screensize'));
        if save_per_subject_graphs
            plot_name = 'normalized_event_times_histogram.png';
            curr_plot_path = [curr_save_dir, '/', plot_name];
            saveas(gcf, curr_plot_path);
        end
        
        % Plot the normalized trajectory means, and means plus standard deviations
        plot_normalized_trajectories(curr_trials, Ns, curr_figtitle, curr_numstddev);
        set(gcf, 'Position', get(0, 'Screensize'));
        if save_per_subject_graphs
            plot_name = 'joint_trajectories_full_cycle.png';
            curr_plot_path = [curr_save_dir, '/', plot_name];
            saveas(gcf, curr_plot_path);
        end
    end
end


%% For each subject's set

% For each subject
for ii = 1 : length(subjects_unique)
    
    % Current subject
    curr_subject = subjects_unique(ii);
    % Subject name
    curr_subject_name = upper(strrep(curr_subject, 'subject_', ''));
    % Find the sets for current subject
    [sets_unique, ~, sets_indices] = unique(set_numbers(subjects_indices==ii));
    
    % For each set
    for jj = 1 : length(sets_unique)
        % Get current subject's trials from current set
        curr_trials = all_trials(subjects_indices' == ii & set_numbers == sets_unique(jj));
        
        % Make a title for current subjects trials
        curr_figtitle = {['Mean and standard deviations for joint trajectories of the lift/lower cycle'];
                         ['for subject ', char(curr_subject_name), ' Set ', num2str(sets_unique(jj), '%d'), ' with ', num2str(length(curr_trials)), ' trials']};
        % Number of standard deviations
        curr_numstddev = 3;

        % Plot the graphs that corncern single subjects
        if plot_per_set_graphs
            % Create the current saving directory
            if save_per_set_graphs
                curr_save_dir = [save_graphs_dir_1, '/', char(subjects_unique(ii)), '/Set', num2str(sets_unique(jj), '%02d')];
                if ~exist(curr_save_dir); mkdir(curr_save_dir); end
            end

            % Plot historams of the event occurence times in the lifting motion and model their distribution as gaussian
            plot_events(curr_trials, Ts, event_names, figrows, figcols);
            set(gcf, 'Position', get(0, 'Screensize'));
            if save_per_set_graphs
                plot_name = 'absolute_event_times_histogram.png';
                curr_plot_path = [curr_save_dir, '/', plot_name];
                saveas(gcf, curr_plot_path);
            end

            % Plot historams of the event occurence normalized times in the lifting motion and model their distribution as gaussian
            plot_normalized_events(curr_trials, event_names, figrows, figcols);
            set(gcf, 'Position', get(0, 'Screensize'));
            if save_per_set_graphs
                plot_name = 'normalized_event_times_histogram.png';
                curr_plot_path = [curr_save_dir, '/', plot_name];
                saveas(gcf, curr_plot_path);
            end

            % Plot the normalized trajectory means, and means plus standard deviations
            plot_normalized_trajectories(curr_trials, Ns, curr_figtitle, curr_numstddev);
            set(gcf, 'Position', get(0, 'Screensize'));
            if save_per_set_graphs
                plot_name = 'joint_trajectories_full_cycle.png';
                curr_plot_path = [curr_save_dir, '/', plot_name];
                saveas(gcf, curr_plot_path);
            end
        end
    end
end
