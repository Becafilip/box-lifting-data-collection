%DATA_ANALYSIS02 loads the estimated trajectories from the input directory:
%'../../processed_data/step02_extract_angles'
%corresponding to the 'exciting' movements. Also loads the Body Segment
%Inertial Parameters estimated by anthropometric tables that are saved
%inside:
%'../../processed_data/subject_dynamical_parameters'
%
%This script calculates the reaction forces that are expected with the
%given Body Segment Inertial Parameters and the kinematics given by the
%'exciting' movements, and compares them to the measured reaction forces.

clear all;
close all;
clc;

%% Add model functions
addpath('../model');

%% Define directories that are in play

subject_movement_data_dir = '../../processed_data/step02_extract_angles';
subject_BSIP_data_dir = '../../processed_data/subject_dynamical_parameters';
processed_data_dir = '../../processed_data/step04_grouped';
% save_graphs_dir_1 = '../../processed_data/graphs/Analysis02_BSIP_GRF';
% if ~exist(processed_data_dir, 'dir'); mkdir(processed_data_dir); end
% if ~exist(save_graphs_dir_1, 'dir'); mkdir(save_graphs_dir_1); end

%% Define the keyword of the subdirectories you want to analyze
keyword = 'exciting';
keyword2 = 'subject';

%% Check input data directory

% Get all within input_data folder (by default two directories with name '.'
% and '..', so we'll start checking items with index 3)
subject_movement_data = dir(subject_movement_data_dir);
subject_BSIP_data = dir(subject_BSIP_data_dir);

if length(subject_movement_data) ~= length(subject_BSIP_data)
    error("Not the same number of subjects in movement and BSIP folder")
end

% Total number of items within it
num_items = length(subject_movement_data);

% Prealocate 

% Go from the first non-default element at index 3
ii0 = 3;
for ii = 3 : num_items
    
    % Get subdirectory name
    subdir_name = subject_movement_data(ii).name;
    
    % Get the ssubdirectory path
    subdir_movement_path = [subject_movement_data_dir, '/', subdir_name];
    subdir_BSIP_path = [subject_BSIP_data_dir, '/', subdir_name];
    
    % Get subsbudirectories
    subsubdirs_movement = dir(subdir_movement_path);
    subsubdirs_BSIP = dir(subdir_BSIP_path);
    
    % Total number of subitems
    num_sub = length(subsubdirs_movement);
    
    % Get the file containing the keyword 
    movement_data_filename = subsubdirs_movement(contains({subsubdirs_movement.name}, keyword)).name;
    BSIP_data_filename = subsubdirs_BSIP(contains({subsubdirs_BSIP.name}, keyword2)).name;
    
    % Load the files
    movement_data_filepath = [subdir_movement_path, '/', movement_data_filename];
    BSIP_data_filepath = [subdir_BSIP_path, '/', BSIP_data_filename];
    
    % Load model param structure with BSIP's
    load(BSIP_data_filepath);
    param = modelParam;
    
    % Load the kinematics & extracted segment leghts in the modelParam
    load(movement_data_filepath);
    
    % Copy over necessary fields
    for fn = fieldnames(modelParam)'
        param.(fn{1}) = modelParam.(fn{1});
    end
    
    % Retransfer the param to modelParam
    modelParam = param;
    
    % Group everything into a structure
    trial = [];
    trial.q = q;
    trial.Markers = Markers;
    trial.Forceplate = Forceplate;
    trial.modelParam = modelParam;
    trial.subject_name = subdir_name;
    trial.session_name = strrep(movement_data_filename, '.mat', '');
    
    
    % Put inside alltrials
    all_trials(ii-ii0+1) = trial;
%     disp(trial.subject_name);
%     disp(trial.session_name);
    % Add to all trials
%     % 
%     disp(movement_data_filepath) p
%     disp(BSIP_data_filepath)
%     disp('\n')
end


% For all trials compare lengths of AT and Measurements
figure;
for jj = 1 : length(all_trials)
    
    % AT lengths
    AT_L = [];
    for ll = 1 : 6
        AT_L = eval(['[AT_L, all_trials(jj).modelParam.L', num2str(ll), '];']);
    end
    
    % Add them as a matrix parameter
    all_trials(jj).modelParam.AT_L = AT_L;
    
    % Set the measured parameters to be the L1,L2,...,L6
    for ll = 1 : 6
        eval(['all_trials(jj).modelParam.L', num2str(ll), ' = all_trials(jj).modelParam.L(', num2str(ll), ');']);
    end
    
    % Plot them on the same graph
    set(groot,'defaultAxesTickLabelInterpreter','latex');  
    subplot(length(all_trials), 1, jj);
    hold on;
    barLocations = 1 : 6;
    barChart = bar(barLocations, [AT_L;all_trials(jj).modelParam.L']);
    colorscheme = jet(2);
    barChart(1).FaceColor = colorscheme(1, :);
    barChart(2).FaceColor = colorscheme(2, :);
    set(barChart, {'DisplayName'}, {'AT'; 'OT'})
    xticks(barLocations);
    xticklabels({'$L_1$', '$L_2$', '$L_3$', '$L_4$', '$L_5$', '$L_6$'});
    if jj == 6; xlabel('$i = 1 , 2, \ldots, 6$', 'Interpreter', 'LaTeX'); end
    ylabel('$L_i$ [m]', 'Interpreter', 'LaTeX');
    if jj==1; legend; end

end


%% Calculate EW

% Define the sampling rate
Ts = 0.01;


for jj = 1 : length(all_trials)
    
    % Get the vel and acc, and time vec
    q = all_trials(jj).q;
%     q = medfilt1(q, 25, [], 2)
%     q = mod(q, 2*pi);
    q = lowpass_filter(q, 1/Ts, 1, 5);
    dq = diff(q, 1, 2) / Ts;
    ddq = diff(q, 2, 2) / Ts^2;

    
    q = q(:, 1:end-2);
    dq = dq(:, 1:end-1);
    
    t = 0 : Ts : (size(q, 2) - 1) * Ts;
    
    % Calculate the joint and base forces and torquest
    EW = zeroExternalWrenches6DOF(size(q,2));
    [G,E] = Dyn_6DOF(q,dq,ddq,all_trials(jj).modelParam,EW);
    
    % Plot and compare to measured forces
    FX_FP = all_trials(jj).Forceplate.Forces(1:end-2, 1)';
    FX_EST = E(1, :);
    
    FY_FP = all_trials(jj).Forceplate.Forces(1:end-2, 2)';
    FY_EST = E(2, :);
    
%     figure;
%     subplot(1,2,2)
%     hold on;
%     plot(t, FX_FP, 'r', 'DisplayName', 'Measure', 'LineWidth', 2);
%     plot(t, FX_EST, 'k', 'DisplayName', 'ID w/ AT');
%     subplot(1,2,1)
%     hold on;
%     plot(t, FY_FP, 'r', 'DisplayName', 'Measure', 'LineWidth', 2);
%     plot(t, FY_EST, 'k', 'DisplayName', 'ID w/ AT');
%     legend;
    figure;
    for rr = 1 : 3
        for cc = 1 : 2
            curr = cc + (rr-1)*2;
            t = linspace(0, 1, size(q,2));
            subplot(3,2,curr)
            plot(t, rad2deg(q(curr, :)));
            xlabel('t[s]');
            ylabel(['\theta_{', num2str(curr), '}(t)']);
        end
    end
end

