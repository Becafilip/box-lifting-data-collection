function J = Step02_angle_id_cf(q,p,base,L)
%ANGLE_ID_CF this is the cost function used for the identification of the
%joint angles. It calculates a vector of displacements along both x and y
%axes between joint locations with current q's and the joint locations
%given by the Markers' and stacked in a matrix p.
%   J = ANGLE_ID_CF(q,p,base,L) is the current guess of joint angles, p is
%   the matrix where each column is the xy position of each joint given by 
%   markers, base is the position of the robot base and L are the segment
%   lengths.


% Find the FKM for the joint angles
T = FKM_nDOF_Tensor(q, L);
 
% Isolate the XY coordinates
p_est = squeeze(T(1:2, 4, :));

% Add the coordinates of the base of the robot
base = reshape(base, [2, 1]);
p_est = p_est + base;

% Find the difference
diffp = p - p_est;

% Output is the vector of differences
J = diffp(:);

end

