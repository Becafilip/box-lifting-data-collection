%This script calculates the BSIP for our 6DOF planar model of the human
%body. It is mostly based on Dumas, 2018 BSIP data. Our model consists of
%the following segments.
% 1. Shank + Foot (2 of them)
% 2. Thigh (2 of them)
% 3. Pelvis + Abdomen
% 4. Thorax
% 5. Upper arm (2 of them)
% 6. Forearm + Hand (2 of them)
clear all;
close all;
clc;

%% Define directories that are in play
bsip_table_path = '../../raw_data/Parameters.xlsx';
input_data_path = '../../raw_data/subjects_data/subjects_info.xlsx';
model_param_path = '../../processed_data/step02_extract_angles';
processed_data_dir = '../../processed_data/subject_dynamical_parameters';
if ~exist(processed_data_dir); mkdir(processed_data_dir); end

%% 

% Read input file
[SubjNum, SubjTxt, SubjRaw] = xlsread(input_data_path);

% Read bsip_table
[TableNum, TableTxt, TableRaw] = xlsread(bsip_table_path);

% Mean subject height from Dumas 2007, 2018
mean_subj_height = 1.77;

% Get all text except the column and row names
DatTxt = TableTxt(2:end, 2:end);

% Convert imaginary numbers from text to double
for ii = 1 : size(TableNum, 1)
    for jj = 1 : size(TableNum, 2)

        % If a NaN is in Num, convert from text to numeric
        if isnan(TableNum(ii, jj))
            TableNum(ii,jj) = str2num(DatTxt{ii, jj});
        end
    end
end

% Traverse 
SubjTxt = SubjTxt(2:end, :);

%%
for subj_cnt = 2 : size(SubjNum, 1)
% for subj_cnt= 2 : 2
    
    % If it's NaN skip it
    if isnan(SubjNum(subj_cnt, 1))
        continue
    end
    
    % If it's not
    subject_name = ['subject_', lower(SubjTxt{subj_cnt, 2})];
    
    %% Section 1
    %Extract geometrical parameters using the table from Dumas, 2018 for
    %male subjects, along with the subject height and weight. The table 
    %parameters are stored in a file called Parametres.xlsx.

    % Specific subject weight and height
    param.body_weight=SubjNum(subj_cnt, 5);       % [kg] 
    param.body_height=SubjNum(subj_cnt, 4)/100;   % [m]

    % Create structures for all the body segments (all rows of the table)
    for ii = 1 : size(TableTxt, 1)-1

        % If current row name contains spaces replace with underscore
        TableTxt{ii+1, 1} = strrep(TableTxt{ii+1, 1}, ' ', '_');

        % Create segment lengths (height * mean seg len / mean subj height)
        L.(TableTxt{ii+1, 1}) = param.body_height*TableNum(ii, 1)/mean_subj_height;

        % Create segment masses (weight * segm mass percentage / 100)
        M.(TableTxt{ii+1, 1}) = param.body_weight*TableNum(ii, 2)/100;

        % Create segment centers of mass in percentages (perc of seg length / 100)
        COM.(TableTxt{ii+1, 1}) = (TableNum(ii, 3:5)/100).'; % Column vector

        % Create segment intertia matrices: 
        % a) Gyration matrix
        G = diag((TableNum(ii, 6:8) / 100).^2) / 2;  % Divide by 2 because of the symmetric addition a few lines down
        G(1, 2) = (TableNum(ii, 9) / 100)^2;
        G(1, 3) = (TableNum(ii, 10) / 100)^ 2;
        G(2, 3) = (TableNum(ii, 11) / 100)^2;
        G = G + G.';    % Make symmetric by addition

        % b) 
        % (seg mass * seg len^2 * gyration matrix in percentages^2 / 100^2)
        IM.(TableTxt{ii+1, 1}) = M.(TableTxt{ii+1, 1}) * L.(TableTxt{ii+1, 1})^2 * G;
    end

    %% Section 2
    % Change the reference frames of the COM and IM for certain segments. The
    % segment reference frames are given within Dumas, 2018, and the calculated
    % IM are given w.r.t. to the segment reference frame orientation, at the
    % COM position.
    % We need a Denavit-Hartenberg type orientation, so all the segment 
    % reference frames need to be oriented such that the X axis goes along the
    % segment. All segment reference frames must be placed at the joint at the 
    % proximal end of the segment.

    % Segments for which segment reference frame should be moved from distal to
    % proximal end
    dtp_segments = {'Shank', 'Thigh', 'Pelvis', 'Abdomen', 'Thorax'};

    % Arm segments, for which the rotation of the segment reference frames needs
    % to be changed differently than for other segments
    arm_segments = {'Upper_Arm', 'Forearm', 'Hand'};

    % Rotate by -90 degrees around the Z axis
    Rz_minus90 = rotz(-90);
    % Rotate by +90 degrees around the Z axis
    Rz_plus90 = rotz(90);

    % Segment names
    segments = fieldnames(COM);

    % For all segments
    for ii = 1 : length(segments)

        % If segment belongs to the set of segments for which we need to
        % change the segment frame position from distal to proximal end
        if ismember(segments{ii}, dtp_segments)
            % Modify the CoM Y-coordinate (expressed in percentages), by adding
            % 1 to it (since we are moving from distal to proximal end, we are
            % effectively moving along a vector in the direction of -y for a
            % distance of the segment length, which in percentages is 1).
            % The X and Z coordinates remain unchanged.
           COM.(segments{ii})(2) = 1 + COM.(segments{ii})(2);
        end

        % If segment belongs to the set of arm segments for which a rotation
        % matrix of +90 degrees is needed
        if ismember(segments{ii}, arm_segments)
            % Adjust orientation of frame in which CoM is expressed
            COM.(segments{ii}) = Rz_plus90 * COM.(segments{ii});

            % Adjust orientation of frame in which the IM is expressed (formula
            % number 5 in Dumas 2018)
            IM.(segments{ii}) = Rz_plus90 * IM.(segments{ii}) * Rz_plus90.';

        % Otherwise use the -90 degrees rotation matrix
        else
            % Adjust orientation of frame in which CoM is expressed
            COM.(segments{ii}) = Rz_minus90 * COM.(segments{ii});

            % Adjust orientation of frame in which the IM is expressed (formula
            % number 5 in Dumas 2018)
            IM.(segments{ii}) = Rz_minus90 * IM.(segments{ii}) * Rz_minus90.';
        end
        % Adjust the position of frame in which the IM is expressed (formula
        % number 6 in Dumas 2018)
        IM.(segments{ii}) = IM.(segments{ii}) + M.(segments{ii}) * ...
        (( (COM.(segments{ii}) * L.(segments{ii})).' * (COM.(segments{ii}) * L.(segments{ii}))) * eye(3) - ...
        (COM.(segments{ii}) * L.(segments{ii})) * (COM.(segments{ii}) * L.(segments{ii})).');
    end

    %% Perform checks:
    % 1. X component of COM >=0 (for all except Foot)
    % 2. Are all IM symmetric and positive definite
    for ii = 1 : length(segments)
        if ~isequal(segments{ii}, 'Foot')
            fprintf('%d', COM.(segments{ii})(1) > 0);
        end

        fprintf('%d', all(eig(IM.(segments{ii}))>0));
    end
    fprintf('\n');

    % Print diagonal elements of IMs
    for ii = 1 : length(segments)
       fprintf('%.3f, %.3f, %.3f\n', diag(IM.(segments{ii}))); 
    end
    
    % Length parameters
    param.L1 = L.Shank;
    param.L2 = L.Thigh;
    param.L3 = L.Pelvis + L.Abdomen;
    param.L4 = L.Thorax;
    param.L5 = L.Upper_Arm;
    param.L6 = L.Forearm;

    % Mass Parameters
    param.M1=2*M.Shank;
    param.M2=2*M.Thigh;
    param.M3=M.Pelvis + M.Abdomen;
    param.M4=M.Thorax + M.Head_with_Neck;
    param.M5=2*M.Upper_Arm;
    param.M6=2*(M.Forearm + M.Hand);

    param.Mtot=param.M1+param.M2+param.M3+param.M4+param.M5+param.M6;
    
    % Center of Mass (COM in percentage of seg. length * seg. length)
    % 1. Shank + Foot (2 of them)
    param.COM1=COM.Shank * L.Shank;
    % 2. Thigh (2 of them)
    param.COM2=COM.Thigh * L.Thigh;
    
    % 3. Pelvis + Abdomen
    % Find COM of Pelvis in Pelvis frame
    PelvisCOM = COM.Pelvis * L.Pelvis;
    % Find COM of Abdomen in Pelvis frame (Translation of L.Pelvis along x axis)
    AbdomenCOM = COM.Abdomen * L.Abdomen + [L.Pelvis;0;0];
    % COM is expressed as overall center of mass
    param.COM3=(PelvisCOM * M.Pelvis + AbdomenCOM * M.Abdomen) / (M.Pelvis + M.Abdomen);
    
    % 4. Thorax + Head and Neck
    % COM of Thorax in Thorax frame
    ThoraxCOM = COM.Thorax * L.Thorax;
    % COM of Head in Thorax frame (Translation of L.Thorax along x axis)
    HeadCOM = COM.Head_with_Neck * L.Head_with_Neck + [L.Thorax;0;0];
    % Overall COM of thorax and head
    param.COM4=(ThoraxCOM * M.Thorax + HeadCOM * M.Head_with_Neck) / (M.Thorax + M.Head_with_Neck);
    
    % 5. Upper arm (2 of them)
    param.COM5=COM.Upper_Arm * L.Upper_Arm;
    
    % 6. Forearm + Hand (2 of them)
    % COM of Forearm in Forearm frame
    ForearmCOM = COM.Forearm * L.Forearm;
    % COM of Hand in Forearm frame (Translation of L.Forearm along x axis)
    HandCOM = COM.Hand * L.Hand + [L.Forearm; 0; 0];
    % Overall COM of Forearm and Hand
    param.COM6=(ForearmCOM * M.Forearm + HandCOM * M.Hand)/(M.Forearm + M.Hand);

    %% Inertia Parameters

    % 1. Shank (2 of them)
    param.ZZ1 = 2*IM.Shank(3,3);
    
    % 2. Thigh (2 of them)
    param.ZZ2 = 2*IM.Thigh(3,3);

    % 3. Pelvis + Abdomen
    % Abdomen IM in Pelvis frame
    r_pelv2abd = [param.L3 / (1 + L.Abdomen / L.Pelvis); 0 ; 0];
    AbdomenIM = IM.Abdomen + M.Abdomen*(r_pelv2abd.'*r_pelv2abd * eye(3) - r_pelv2abd * r_pelv2abd.');
    % Add Abdomen and Pelvis IM
    Seg3IM = AbdomenIM + IM.Pelvis;
    % Extract moment of inertia around Z
    param.ZZ3 = Seg3IM(3,3);

    % 4. Thorax + Head
    % Head IM in the Thorax Frame
    r_thor2head = [param.L4;0;0];
    HeadIM = IM.Head_with_Neck + M.Head_with_Neck*(r_thor2head.'*r_thor2head*eye(3)-r_thor2head * r_thor2head.');
    % Add Thorax and Head IM
    Seg4IM = HeadIM + IM.Thorax;
    % Extract moment of inertia around Z
    param.ZZ4 = Seg4IM(3,3);

    % 5. Upper arm (2 of them)
    param.ZZ5 = 2*IM.Upper_Arm(3,3);

    % 6. Forearm + Hand (2 of them)
    % Hand IM in the Forearm Frame
    r_fore2hand = [param.L6;0;0];
    HandIM = IM.Hand + M.Hand*(r_fore2hand.'*r_fore2hand*eye(3) + r_fore2hand*r_fore2hand.');
    % Add Forearm and Hand IM
    Seg6IM = HandIM + IM.Forearm;
    % Exctract moment of inertia around Z
    param.ZZ6 = Seg6IM(3,3);
    
    % Get the directory to save
    save_dir = [processed_data_dir, '/', subject_name];
    if ~exist(save_dir); mkdir(save_dir); end
    % Get the save file path
    save_file_path = [save_dir, '/', subject_name, '.mat'];
    
    modelParam = param;
    save(save_file_path, 'modelParam');
    disp(save_file_path)    
end

