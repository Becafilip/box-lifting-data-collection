function trials = trial_event_indexing(all_trials, event_ind1, event_ind2)
%TRIAL_EVENT_INDEXING takes in the structure array of trials and extracts
%the trajectories between two events given by their event index.
%
%   trials = TRIAL_EVENT_INDEXING(all_trials, event_ind1, event_ind2) 
%   extracts the part of the trials trajectory between event1 and event2
%   which are given by their event index.

trials = struct([]);

for ii = 1 : length(all_trials)    
    % Copy over all the fieldnames
    trialfn = fieldnames(all_trials);
    for ff = 1 : length(trialfn)
        trials(ii).(trialfn{ff}) = all_trials(ii).(trialfn{ff});
    end
    
    % Get the trajectory index the event refers to
    event1 = all_trials(ii).events(event_ind1);
    event2 = all_trials(ii).events(event_ind2);
    
    % For the event, trajectory, marker and forceplate data fields change
    % them
    % Trajectory
    trials(ii).q = all_trials(ii).q(:, event1:event2);
    % Markerset
    Msetnames = fieldnames(all_trials(ii).Markers);
    for mm = 1 : length(Msetnames)
        Mnames = fieldnames(all_trials(ii).Markers.(Msetnames{mm}));
        for nn = 1 : length(Mnames)
           trials(ii).Markers.(Msetnames{mm}).(Mnames{nn}) = all_trials(ii).Markers.(Msetnames{mm}).(Mnames{nn})(event1:event2, :);
        end      
    end
    % Forceplate
    Fnames = fieldnames(all_trials(ii).Forceplate);
    for ff = 1 : length(Fnames)
        trials(ii).Forceplate.(Fnames{ff}) = all_trials(ii).Forceplate.(Fnames{ff})(event1:event2, :);
    end
    % Events (keep only relevant ones, and renormalize index of event1 to 1)
    trials(ii).events = all_trials(ii).events(event_ind1:event_ind2);
    trials(ii).events = trials(ii).events - trials(ii).events(1) + 1;
end


end

