%DATA_ANALYSIS03 loads the grouped trajectories from the directory:
%'../../processed_data/step05_lifting_parameters'
%which are grouped into either 'normal' or 'squat' files/structures.
%This script plots some statistical, and other information concerning those
%trajectories in the goal of finding some invariant characteristics across
%the trials.
%saves those graphs according to a flag into the directory
%'../../processed_data/graphs/Analysis03'
%   
clear all; 
close all;
clc;


%% Add model functions
addpath('../model');
addpath('../other');
addpath('../animation_functions')

%% Define the keyword of the subdirectories you want to analyze
keyword_lift = 'normal';
% keyword_dir = 'squat';

%% Define directories that are in play

input_file = ['../../processed_data/step05_lifting_parameters/', keyword_lift, '_trials.mat'];
save_path = '../../processed_data/graphs/Analysis03';
if ~exist(save_path, 'dir'); mkdir(save_path); end

% Load the input file
load(input_file)

% Flags
save_hand_graphs = false;
save_com_graphs = true;


%% Extract the lifting part of the trajectories

% Extract the lifting part of the trials (between event 3 and 4)
all_trials_lift = trial_event_indexing(all_trials, 3, 4);

% Clear all_trials to avoid confusion
clear all_trials
clear all_trials_statistics

%%
% % Test if trajectories are well extracted
% for ii = 1 : length(all_trials_lift)
%     if mod(ii, 30) == 0
%         figure
%         Animate_nDOF(all_trials_lift(ii).q, all_trials_lift(ii).modelParam.L, 0.01);
%         pause
%         close
%     end
% end

% Get the timing of the events
t_events = zeros(length(all_trials_lift), size(all_trials_lift(1).events, 2));
for ii = 1 : length(all_trials_lift)
    t_events(ii, :) = all_trials_lift(ii).events;
end

% Mean and median timing
mean_t_events = mean(t_events);
medi_t_events = median(t_events);
% Median numer of samples
Ns = medi_t_events(end);

% Resample all the trajectories with the same number of samples
all_trajectories_lift = zeros(6, Ns, length(all_trials_lift));
for ii = 1 : length(all_trials_lift)
    all_trajectories_lift(:, :, ii) = spline_resample(all_trials_lift(ii).q, Ns);
end

% Calculate hand trajectories
hand_traj = zeros(2, Ns, length(all_trials_lift));
com_traj = zeros(2, Ns, length(all_trials_lift));
for ii = 1 : length(all_trials_lift)
    % Get FKM
    FKM = FKM_nDOF_Tensor(squeeze(all_trajectories_lift(:,:,ii)), all_trials_lift(ii).modelParam.L);
    % Get hand traj
    hand_traj(:,:,ii) = squeeze(FKM(1:2, 4, end, :));
    % Get COM
    COM = COM_nDOF_Tensor(squeeze(all_trajectories_lift(:,:,ii)), all_trials_lift(ii).modelParam.L, all_trials_lift(ii).modelParam.M/all_trials_lift(ii).modelParam.Mtot, all_trials_lift(ii).modelParam.COM);
    % Get com traj
    com_traj(:, :, ii) = COM(1:2, :);
end

% Normalized time
t_norm = linspace(0, 1, Ns);

% % Test if trajectories are well interpolated
% for ii = 1 : length(all_trials_lift)
%     if mod(ii, 30) == 0
%         figure
%         Animate_nDOF(squeeze(all_trajectories_lift(:, :, ii)), all_trials_lift(ii).modelParam.L, 0.01);
%         pause
%         close
%     end
% end

% Plot the trajectories and also the position in the XY plane
figure;
hold all;
plot(t_norm, squeeze(hand_traj(1, :, :))');
xlabel('Time [norm.]');
ylabel('X-position [m]');
title(['The X-position of the hand across time, sample size ', num2str(length(all_trials_lift))]);

figname = 'Hand-X-position.png';
output_file = [save_path, '/', figname];
if save_hand_graphs; saveas(gcf, output_file); end

figure;
hold all;
plot(t_norm, squeeze(hand_traj(2, :, :))');
xlabel('Time [norm.]');
ylabel('Y-position [m]');
title(['The Y-position of the hand across time, sample size ', num2str(length(all_trials_lift))]);

figname = 'Hand-Y-position.png';
output_file = [save_path, '/', figname];
if save_hand_graphs; saveas(gcf, output_file); end

figure;
hold all;
for ii = 1 : length(all_trials_lift)
    plot(squeeze(hand_traj(1, :, ii))', squeeze(hand_traj(2, :, ii))');
    rectangle('Position', all_trials_lift(ii).LiftParam.TableRectangle);
end
xlabel('X-position [m]');
ylabel('Y-position [m]');
title(['The XY-path of the hand across time, sample size ', num2str(length(all_trials_lift))]);

figname = 'Hand-XY-path.png';
output_file = [save_path, '/', figname];
if save_hand_graphs; saveas(gcf, output_file); end


% Plot the COM trajectories and also the path in the XY plane
figure;
hold all;
plot(t_norm, squeeze(com_traj(1, :, :))');
xlabel('Time [norm.]');
ylabel('X-position [m]');
title(['The X-position of the COM across time, sample size ', num2str(length(all_trials_lift))]);

figname = 'COM-X-position.png';
output_file = [save_path, '/', figname];
if save_com_graphs; saveas(gcf, output_file); end

figure;
hold all;
plot(t_norm, squeeze(com_traj(2, :, :))');
xlabel('Time [norm.]');
ylabel('Y-position [m]');
title(['The Y-position of the COM across time, sample size ', num2str(length(all_trials_lift))]);

figname = 'COM-Y-position.png';
output_file = [save_path, '/', figname];
if save_com_graphs; saveas(gcf, output_file); end

figure;
hold all;
for ii = 1 : length(all_trials_lift)
    plot(squeeze(com_traj(1, :, ii))', squeeze(com_traj(2, :, ii))');
end
xlabel('X-position [m]');
ylabel('Y-position [m]');
title(['The XY-path of the COM across time, sample size ', num2str(length(all_trials_lift))]);

figname = 'COM-XY-path.png';
output_file = [save_path, '/', figname];
if save_com_graphs; saveas(gcf, output_file); end


names = unique(arrayfun(@(elem) elem.subject_name, all_trials_lift, 'UniformOutput', false));
names = strrep(names, '_', ' ');
figure;
hold all;
for ii = 1 : length(all_trials_lift)
    if mod(ii-1, 20) == 0
        plot(squeeze(com_traj(1, :, ii))', squeeze(com_traj(2, :, ii))', 'DisplayName', names{floor((ii-1) / 20 + 1)});
    end
end
xlabel('X-position [m]');
ylabel('Y-position [m]');
title(['The XY-path of the COM across time, one trial per subject']);
legend('Location', 'Best');

figname = 'COM-XY-path-reduced-multiple-subjects.png';
output_file = [save_path, '/', figname];
if save_com_graphs; saveas(gcf, output_file); end

num_subj = length(names);
for jj = 1 : num_subj
    figure;
    hold all;
    for ii = 1 : length(all_trials_lift)
        if ii > (jj-1)*20 && ii < jj * 20 && mod(ii-1, 5) == 0
            plot(squeeze(com_traj(1, :, ii))', squeeze(com_traj(2, :, ii))', 'DisplayName', ['Set ', num2str(floor((ii - (jj-1)*20 - 1) / 5 + 1))]);
        end
    end
    xlabel('X-position [m]');
    ylabel('Y-position [m]');
    title(['The XY-path of the COM across time of ', char(names{jj}), ', one trial per set']);
    legend('Location', 'Best');
    
    figname = ['COM-XY-path-reduced-', char(names{jj}),'.png'];
    output_file = [save_path, '/', figname];
    if save_com_graphs; saveas(gcf, output_file); end
end
