%STEP05_PROCESS loads the entirely processed and group data given inside
%the directory:
%'../../processed_data/step04_all_trials'
%and loads either the normal or squat trials stored in a structure array.
%This script calculates additional parameters of the lifting and saves them
%inside a structure called liftParam and inserts that structure inside each
%element of the structure array.
%The results are saved inside:
%'../../processed_data/step05_lifting_parameters'
%either under the name normal_trials or squat_trials.

clear all;
close all;
clc;

% Add model functions
addpath('../model');
addpath('../other');
addpath('../animation_functions');

%% Keywords
% keyword_lift = 'squat';
keyword_lift = 'normal';

%% Define directories that are in play

input_file = ['../../processed_data/step04_all_trials/', keyword_lift, '_trials.mat'];
save_path = '../../processed_data/step05_lifting_parameters';
save_filename = [keyword_lift, '_trials.mat'];
output_file = [save_path, '/', save_filename];
if ~exist(save_path, 'dir'); mkdir(save_path); end

load(input_file);

%% Add flags

save_data_flag = true;

%% Get the table height across the trials

% Table Markers
farl_m = zeros(length(all_trials), 3);
farr_m = zeros(length(all_trials), 3);
near_m = zeros(length(all_trials), 3);

farl_var = zeros(length(all_trials), 3);
farr_var = zeros(length(all_trials), 3);
near_var = zeros(length(all_trials), 3);

farl_2_base_m = zeros(length(all_trials), 3);
farr_2_base_m = zeros(length(all_trials), 3);
near_2_base_m = zeros(length(all_trials), 3);

farl_2_base_var = zeros(length(all_trials), 3);
farr_2_base_var = zeros(length(all_trials), 3);
near_2_base_var = zeros(length(all_trials), 3);

% Box Markers
box_neadr_m = zeros(length(all_trials), 3);
box_fardl_m = zeros(length(all_trials), 3);
box_fardr_m = zeros(length(all_trials), 3);
box_farur_m = zeros(length(all_trials), 3);

box_neadr_var = zeros(length(all_trials), 3);
box_fardl_var = zeros(length(all_trials), 3);
box_fardr_var = zeros(length(all_trials), 3);
box_farur_var = zeros(length(all_trials), 3);

box_neadr_2_base_m = zeros(length(all_trials), 3);
box_fardl_2_base_m = zeros(length(all_trials), 3);
box_fardr_2_base_m = zeros(length(all_trials), 3);
box_farur_2_base_m = zeros(length(all_trials), 3);

box_neadr_2_base_var = zeros(length(all_trials), 3);
box_fardl_2_base_var = zeros(length(all_trials), 3);
box_fardr_2_base_var = zeros(length(all_trials), 3);
box_farur_2_base_var = zeros(length(all_trials), 3);

% 
box_2_wrist_m = zeros(length(all_trials), 3);
box_2_wrist_var = zeros(length(all_trials), 3);


for ii = 1 : length(all_trials)
    % Table Marker positions (bruto and w.r.t. to body frame)
    farl_m(ii, :) = mean(all_trials(ii).Markers.TABLE.FARL);
    farr_m(ii, :) = mean(all_trials(ii).Markers.TABLE.FARR);
    near_m(ii, :) = mean(all_trials(ii).Markers.TABLE.NEAR);

    farl_var(ii, :) = std(all_trials(ii).Markers.TABLE.FARL);
    farr_var(ii, :) = std(all_trials(ii).Markers.TABLE.FARR);
    near_var(ii, :) = std(all_trials(ii).Markers.TABLE.NEAR);
    
    farl_2_base_m(ii, :) = mean(all_trials(ii).Markers.TABLE.FARL - all_trials(ii).Markers.BODY.RANK);
    farr_2_base_m(ii, :) = mean(all_trials(ii).Markers.TABLE.FARR - all_trials(ii).Markers.BODY.RANK);
    near_2_base_m(ii, :) = mean(all_trials(ii).Markers.TABLE.NEAR - all_trials(ii).Markers.BODY.RANK);

    farl_2_base_var(ii, :) = std(all_trials(ii).Markers.TABLE.FARL - all_trials(ii).Markers.BODY.RANK);
    farr_2_base_var(ii, :) = std(all_trials(ii).Markers.TABLE.FARR - all_trials(ii).Markers.BODY.RANK);
    near_2_base_var(ii, :) = std(all_trials(ii).Markers.TABLE.NEAR - all_trials(ii).Markers.BODY.RANK);
    
    % Box Marker positions (bruto and w.r.t. body frame) before lift
    ind_b4_lift = 1 : all_trials(ii).events(2);
    box_neadr_m(ii, :) = mean(all_trials(ii).Markers.BOX.NEADR(ind_b4_lift, :));
    box_fardl_m(ii, :) = mean(all_trials(ii).Markers.BOX.FARDL(ind_b4_lift, :));
    box_fardr_m(ii, :) = mean(all_trials(ii).Markers.BOX.FARDR(ind_b4_lift, :));
    box_farur_m(ii, :) = mean(all_trials(ii).Markers.BOX.FARUR(ind_b4_lift, :));

    box_neadr_var(ii, :) = std(all_trials(ii).Markers.BOX.NEADR(ind_b4_lift, :));
    box_fardl_var(ii, :) = std(all_trials(ii).Markers.BOX.FARDL(ind_b4_lift, :));
    box_fardr_var(ii, :) = std(all_trials(ii).Markers.BOX.FARDR(ind_b4_lift, :));
    box_farur_var(ii, :) = std(all_trials(ii).Markers.BOX.FARUR(ind_b4_lift, :));
    
    box_neadr_2_base_m(ii, :) = mean(all_trials(ii).Markers.BOX.NEADR(ind_b4_lift, :) - all_trials(ii).Markers.BODY.RANK(ind_b4_lift, :));
    box_fardl_2_base_m(ii, :) = mean(all_trials(ii).Markers.BOX.FARDL(ind_b4_lift, :) - all_trials(ii).Markers.BODY.RANK(ind_b4_lift, :));
    box_fardr_2_base_m(ii, :) = mean(all_trials(ii).Markers.BOX.FARDR(ind_b4_lift, :) - all_trials(ii).Markers.BODY.RANK(ind_b4_lift, :));
    box_farur_2_base_m(ii, :) = mean(all_trials(ii).Markers.BOX.FARUR(ind_b4_lift, :) - all_trials(ii).Markers.BODY.RANK(ind_b4_lift, :));

    box_neadr_2_base_var(ii, :) = std(all_trials(ii).Markers.BOX.NEADR(ind_b4_lift, :) - all_trials(ii).Markers.BODY.RANK(ind_b4_lift, :));
    box_fardl_2_base_var(ii, :) = std(all_trials(ii).Markers.BOX.FARDL(ind_b4_lift, :) - all_trials(ii).Markers.BODY.RANK(ind_b4_lift, :));
    box_fardr_2_base_var(ii, :) = std(all_trials(ii).Markers.BOX.FARDR(ind_b4_lift, :) - all_trials(ii).Markers.BODY.RANK(ind_b4_lift, :));
    box_farur_2_base_var(ii, :) = std(all_trials(ii).Markers.BOX.FARUR(ind_b4_lift, :) - all_trials(ii).Markers.BODY.RANK(ind_b4_lift, :));
    
    % Box Marker Positions w.r.t. wrist in wrist frame
    ind_dur_lift = all_trials(ii).events(3) : all_trials(ii).events(4);
    box_center = (all_trials(ii).Markers.BOX.NEADR(ind_dur_lift, :) + all_trials(ii).Markers.BOX.FARDR(ind_dur_lift, :)) / 2;
    wrist_pos = all_trials(ii).Markers.BODY.RWRI(ind_dur_lift, :);
    
    box_2_wrist_m(ii, :) = mean(wrist_pos - box_center);
    box_2_wrist_var(ii, :) = std(wrist_pos - box_center);
    
    % Create Lift Param structure
    LiftParam = [];
    LiftParam.BoxWidth = 0.33;    % 33cm
    LiftParam.BoxHeight = 0.248;  % 24,8 cm
    LiftParam.BoxMass = 10.5;     % 10,5 kg
    LiftParam.Gravity = 9.81;     % 9,81 m/s^2
    
    LiftParam.TableWidth = farr_2_base_m(ii, 1) - near_2_base_m(ii, 1);
    LiftParam.TableHeight = mean([farl_2_base_m(ii, 2); farr_2_base_m(ii, 2); near_2_base_m(ii, 2);]);
    LiftParam.TableLowerNearCorner = [near_2_base_m(ii, 1), 0, near_2_base_m(ii, 3)];    % Same xz as near, but zero height (y)
    LiftParam.TableRectangle = [LiftParam.TableLowerNearCorner(1:2), LiftParam.TableWidth, LiftParam.TableHeight];
    
    LiftParam.BoxToWristVectorDuringLift = box_2_wrist_m(ii, :).';
    LiftParam.BoxRectangleInitial = [box_neadr_2_base_m(ii, 1:2), LiftParam.BoxWidth, LiftParam.BoxHeight].';
    
    final_index = all_trials(ii).events(end);
    
    LiftParam.PercentageLiftOff = all_trials(ii).events(3) / final_index;
    LiftParam.PercentageDropOff = all_trials(ii).events(4) / final_index;
    
    Twlo = FKM_nDOF_Tensor(all_trials(ii).q(:, all_trials(ii).events(3)), all_trials(ii).modelParam.L);
    Twdo = FKM_nDOF_Tensor(all_trials(ii).q(:, all_trials(ii).events(4)), all_trials(ii).modelParam.L);
    
    LiftParam.WristPositionLiftOff = Twlo(1:3, 4, end);
    LiftParam.WristPositionDropOff = Twlo(1:3, 4, end);
    LiftParam.InitialAngles = all_trials(ii).q(:, 1);
    LiftParam.FinalAngles = all_trials(ii).q(:, final_index);
    
    LiftParam.HeelPosition = mean(all_trials(ii).Markers.BODY.RHEE - all_trials(ii).Markers.BODY.RANK);
    LiftParam.ToePosition = mean(all_trials(ii).Markers.BODY.RTOE - all_trials(ii).Markers.BODY.RANK);
    LiftParam.HeelPosition = LiftParam.HeelPosition(1);
    LiftParam.ToePosition =  LiftParam.ToePosition(1);
    
    % Add
    all_trials(ii).LiftParam = LiftParam;
    all_trials(ii).modelParam.TorqueLimits = 200 * ones(1, 6);
    all_trials(ii).modelParam.JointLimits = [-2*pi * ones(1, 6); 2*pi * ones(1, 6)];
end

% Save the modifications
if save_data_flag; save(output_file, 'all_trials', 'all_trials_statistics'); end

n_std = 3;
figure;
sgtitle('Table Markers in Optitrack Frame');
set(groot,'defaultAxesTickLabelInterpreter','latex'); 
set(groot,'defaulttextinterpreter','latex');
set(groot,'defaultLegendInterpreter','latex');
subplot(3,2,1)
hold on;
m = farl_m(:, 1);
mps = farl_m(:, 1) + n_std * farl_var(:, 1);
mms = farl_m(:, 1) - n_std * farl_var(:, 1);
mstr = '$m_{FARL, x}$';
sstr = ['$m_{FARL, x} \pm ', num2str(n_std) , '\sigma_{FARL, x}$'];
tstr = 'X-Coordinate of Table FARL Marker';
plot(m, 'k', 'DisplayName', mstr);
plot(mps, 'r', 'DisplayName', sstr);
plot(mms, 'r', 'HandleVisibility', 'Off');
xlabel('Trials');
ylabel('Position [m]');
title(tstr);
legend;

subplot(3,2,2)
hold on;
m = farl_m(:, 2);
mps = farl_m(:, 2) + n_std * farl_var(:, 2);
mms = farl_m(:, 2) - n_std * farl_var(:, 2);
mstr = '$m_{FARL, y}$';
sstr = ['$m_{FARL, y} \pm ', num2str(n_std) , '\sigma_{FARL, y}$'];
tstr = 'Y-Coordinate of Table FARL Marker';
plot(m, 'k', 'DisplayName', mstr);
plot(mps, 'r', 'DisplayName', sstr);
plot(mms, 'r', 'HandleVisibility', 'Off');
xlabel('Trials');
ylabel('Position [m]');
title(tstr);
legend;


subplot(3,2,3)
hold on;
m = farr_m(:, 1);
mps = farr_m(:, 1) + n_std * farr_var(:, 1);
mms = farr_m(:, 1) - n_std * farr_var(:, 1);
mstr = '$m_{FARR, x}$';
sstr = ['$m_{FARR, x} \pm ', num2str(n_std) , '\sigma_{FARR, x}$'];
tstr = 'X-Coordinate of Table FARR Marker';
plot(m, 'k', 'DisplayName', mstr);
plot(mps, 'r', 'DisplayName', sstr);
plot(mms, 'r', 'HandleVisibility', 'Off');
xlabel('Trials');
ylabel('Position [m]');
title(tstr);
legend;

subplot(3,2,4)
hold on;
m = farr_m(:, 2);
mps = farr_m(:, 2) + n_std * farr_var(:, 2);
mms = farr_m(:, 2) - n_std * farr_var(:, 2);
mstr = '$m_{FARR, y}$';
sstr = ['$m_{FARR, y} \pm ', num2str(n_std) , '\sigma_{FARR, y}$'];
tstr = 'Y-Coordinate of Table FARR Marker';
plot(m, 'k', 'DisplayName', mstr);
plot(mps, 'r', 'DisplayName', sstr);
plot(mms, 'r', 'HandleVisibility', 'Off');
xlabel('Trials');
ylabel('Position [m]');
title(tstr);
legend;



subplot(3,2,5)
hold on;
m = near_m(:, 1);
mps = near_m(:, 1) + n_std * near_var(:, 1);
mms = near_m(:, 1) - n_std * near_var(:, 1);
mstr = '$m_{NEAR, x}$';
sstr = ['$m_{NEAR, x} \pm ', num2str(n_std) , '\sigma_{NEAR, x}$'];
tstr = 'X-Coordinate of Table NEAR Marker';
plot(m, 'k', 'DisplayName', mstr);
plot(mps, 'r', 'DisplayName', sstr);
plot(mms, 'r', 'HandleVisibility', 'Off');
xlabel('Trials');
ylabel('Position [m]');
title(tstr);
legend;

subplot(3,2,6)
hold on;
m = near_m(:, 2);
mps = near_m(:, 2) + n_std * near_var(:, 2);
mms = near_m(:, 2) - n_std * near_var(:, 2);
mstr = '$m_{NEAR, y}$';
sstr = ['$m_{NEAR, y} \pm ', num2str(n_std) , '\sigma_{NEAR, y}$'];
tstr = 'Y-Coordinate of Table NEAR Marker';
plot(m, 'k', 'DisplayName', mstr);
plot(mps, 'r', 'DisplayName', sstr);
plot(mms, 'r', 'HandleVisibility', 'Off');
xlabel('Trials');
ylabel('Position [m]');
title(tstr);
legend;


%%

n_std = 3;
figure;
sgtitle('Table Markers in Human Base Frame');
set(groot,'defaultAxesTickLabelInterpreter','latex'); 
set(groot,'defaulttextinterpreter','latex');
set(groot,'defaultLegendInterpreter','latex');
subplot(3,2,1)
hold on;
m = farl_2_base_m(:, 1);
mps = farl_2_base_m(:, 1) + n_std * farl_2_base_var(:, 1);
mms = farl_2_base_m(:, 1) - n_std * farl_2_base_var(:, 1);
mstr = '$m_{FARL, x}$';
sstr = ['$m_{FARL, x} \pm ', num2str(n_std) , '\sigma_{FARL, x}$'];
tstr = 'X-Coordinate of Table FARL Marker';
plot(m, 'k', 'DisplayName', mstr);
plot(mps, 'r', 'DisplayName', sstr);
plot(mms, 'r', 'HandleVisibility', 'Off');
xlabel('Trials');
ylabel('Position [m]');
title(tstr);
legend;

subplot(3,2,2)
hold on;
m = farl_2_base_m(:, 2);
mps = farl_2_base_m(:, 2) + n_std * farl_2_base_var(:, 2);
mms = farl_2_base_m(:, 2) - n_std * farl_2_base_var(:, 2);
mstr = '$m_{FARL, y}$';
sstr = ['$m_{FARL, y} \pm ', num2str(n_std) , '\sigma_{FARL, y}$'];
tstr = 'Y-Coordinate of Table FARL Marker';
plot(m, 'k', 'DisplayName', mstr);
plot(mps, 'r', 'DisplayName', sstr);
plot(mms, 'r', 'HandleVisibility', 'Off');
xlabel('Trials');
ylabel('Position [m]');
title(tstr);
legend;


subplot(3,2,3)
hold on;
m = farr_2_base_m(:, 1);
mps = farr_2_base_m(:, 1) + n_std * farr_2_base_var(:, 1);
mms = farr_2_base_m(:, 1) - n_std * farr_2_base_var(:, 1);
mstr = '$m_{FARR, x}$';
sstr = ['$m_{FARR, x} \pm ', num2str(n_std) , '\sigma_{FARR, x}$'];
tstr = 'X-Coordinate of Table FARR Marker';
plot(m, 'k', 'DisplayName', mstr);
plot(mps, 'r', 'DisplayName', sstr);
plot(mms, 'r', 'HandleVisibility', 'Off');
xlabel('Trials');
ylabel('Position [m]');
title(tstr);
legend;

subplot(3,2,4)
hold on;
m = farr_2_base_m(:, 2);
mps = farr_2_base_m(:, 2) + n_std * farr_2_base_var(:, 2);
mms = farr_2_base_m(:, 2) - n_std * farr_2_base_var(:, 2);
mstr = '$m_{FARR, y}$';
sstr = ['$m_{FARR, y} \pm ', num2str(n_std) , '\sigma_{FARR, y}$'];
tstr = 'Y-Coordinate of Table FARR Marker';
plot(m, 'k', 'DisplayName', mstr);
plot(mps, 'r', 'DisplayName', sstr);
plot(mms, 'r', 'HandleVisibility', 'Off');
xlabel('Trials');
ylabel('Position [m]');
title(tstr);
legend;



subplot(3,2,5)
hold on;
m = near_2_base_m(:, 1);
mps = near_2_base_m(:, 1) + n_std * near_2_base_var(:, 1);
mms = near_2_base_m(:, 1) - n_std * near_2_base_var(:, 1);
mstr = '$m_{NEAR, x}$';
sstr = ['$m_{NEAR, x} \pm ', num2str(n_std) , '\sigma_{NEAR, x}$'];
tstr = 'X-Coordinate of Table NEAR Marker';
plot(m, 'k', 'DisplayName', mstr);
plot(mps, 'r', 'DisplayName', sstr);
plot(mms, 'r', 'HandleVisibility', 'Off');
xlabel('Trials');
ylabel('Position [m]');
title(tstr);
legend;

subplot(3,2,6)
hold on;
m = near_2_base_m(:, 2);
mps = near_2_base_m(:, 2) + n_std * near_2_base_var(:, 2);
mms = near_2_base_m(:, 2) - n_std * near_2_base_var(:, 2);
mstr = '$m_{NEAR, y}$';
sstr = ['$m_{NEAR, y} \pm ', num2str(n_std) , '\sigma_{NEAR, y}$'];
tstr = 'Y-Coordinate of Table NEAR Marker';
plot(m, 'k', 'DisplayName', mstr);
plot(mps, 'r', 'DisplayName', sstr);
plot(mms, 'r', 'HandleVisibility', 'Off');
xlabel('Trials');
ylabel('Position [m]');
title(tstr);
legend;

%%

n_std = 3;
figure;
sgtitle('Box Markers (before lift) in Optitrack Frame');
set(groot,'defaultAxesTickLabelInterpreter','latex'); 
set(groot,'defaulttextinterpreter','latex');
set(groot,'defaultLegendInterpreter','latex');
subplot(4,2,1)
hold on;
m = box_fardl_m(:, 1);
mps = box_fardl_m(:, 1) + n_std * box_fardl_var(:, 1);
mms = box_fardl_m(:, 1) - n_std * box_fardl_var(:, 1);
mstr = '$m_{FARDL, x}$';
sstr = ['$m_{FARDL, x} \pm ', num2str(n_std) , '\sigma_{FARDL, x}$'];
tstr = 'X-Coordinate of BOX FARDL Marker';
plot(m, 'k', 'DisplayName', mstr);
plot(mps, 'r', 'DisplayName', sstr);
plot(mms, 'r', 'HandleVisibility', 'Off');
xlabel('Trials');
ylabel('Position [m]');
title(tstr);
legend;

subplot(4,2,2)
hold on;
m = box_fardl_m(:, 2);
mps = box_fardl_m(:, 2) + n_std * box_fardl_var(:, 2);
mms = box_fardl_m(:, 2) - n_std * box_fardl_var(:, 2);
mstr = '$m_{FARDL, y}$';
sstr = ['$m_{FARDL, y} \pm ', num2str(n_std) , '\sigma_{FARDL, y}$'];
tstr = 'Y-Coordinate of BOX FARDL Marker';
plot(m, 'k', 'DisplayName', mstr);
plot(mps, 'r', 'DisplayName', sstr);
plot(mms, 'r', 'HandleVisibility', 'Off');
xlabel('Trials');
ylabel('Position [m]');
title(tstr);
legend;


subplot(4,2,3)
hold on;
m = box_fardr_m(:, 1);
mps = box_fardr_m(:, 1) + n_std * box_fardr_var(:, 1);
mms = box_fardr_m(:, 1) - n_std * box_fardr_var(:, 1);
mstr = '$m_{FARDR, x}$';
sstr = ['$m_{FARDR, x} \pm ', num2str(n_std) , '\sigma_{FARDR, x}$'];
tstr = 'X-Coordinate of BOX FARDR Marker';
plot(m, 'k', 'DisplayName', mstr);
plot(mps, 'r', 'DisplayName', sstr);
plot(mms, 'r', 'HandleVisibility', 'Off');
xlabel('Trials');
ylabel('Position [m]');
title(tstr);
legend;

subplot(4,2,4)
hold on;
m = box_fardr_m(:, 2);
mps = box_fardr_m(:, 2) + n_std * box_fardr_var(:, 2);
mms = box_fardr_m(:, 2) - n_std * box_fardr_var(:, 2);
mstr = '$m_{FARDR, y}$';
sstr = ['$m_{FARDR, y} \pm ', num2str(n_std) , '\sigma_{FARDR, y}$'];
tstr = 'Y-Coordinate of BOX FARDR Marker';
plot(m, 'k', 'DisplayName', mstr);
plot(mps, 'r', 'DisplayName', sstr);
plot(mms, 'r', 'HandleVisibility', 'Off');
xlabel('Trials');
ylabel('Position [m]');
title(tstr);
legend;



subplot(4,2,5)
hold on;
m = box_farur_m(:, 1);
mps = box_farur_m(:, 1) + n_std * box_farur_var(:, 1);
mms = box_farur_m(:, 1) - n_std * box_farur_var(:, 1);
mstr = '$m_{FARUR, x}$';
sstr = ['$m_{FARUR, x} \pm ', num2str(n_std) , '\sigma_{FARUR, x}$'];
tstr = 'X-Coordinate of BOX FARUR Marker';
plot(m, 'k', 'DisplayName', mstr);
plot(mps, 'r', 'DisplayName', sstr);
plot(mms, 'r', 'HandleVisibility', 'Off');
xlabel('Trials');
ylabel('Position [m]');
title(tstr);
legend;

subplot(4,2,6)
hold on;
m = box_farur_m(:, 2);
mps = box_farur_m(:, 2) + n_std * box_farur_var(:, 2);
mms = box_farur_m(:, 2) - n_std * box_farur_var(:, 2);
mstr = '$m_{FARUR, y}$';
sstr = ['$m_{FARUR, y} \pm ', num2str(n_std) , '\sigma_{FARUR, y}$'];
tstr = 'Y-Coordinate of BOX FARUR Marker';
plot(m, 'k', 'DisplayName', mstr);
plot(mps, 'r', 'DisplayName', sstr);
plot(mms, 'r', 'HandleVisibility', 'Off');
xlabel('Trials');
ylabel('Position [m]');
title(tstr);
legend;

subplot(4,2,7)
hold on;
m = box_neadr_m(:, 1);
mps = box_neadr_m(:, 1) + n_std * box_neadr_var(:, 1);
mms = box_neadr_m(:, 1) - n_std * box_neadr_var(:, 1);
mstr = '$m_{NEADR, x}$';
sstr = ['$m_{NEADR, x} \pm ', num2str(n_std) , '\sigma_{NEADR, x}$'];
tstr = 'X-Coordinate of BOX NEADR Marker';
plot(m, 'k', 'DisplayName', mstr);
plot(mps, 'r', 'DisplayName', sstr);
plot(mms, 'r', 'HandleVisibility', 'Off');
xlabel('Trials');
ylabel('Position [m]');
title(tstr);
legend;

subplot(4,2,8)
hold on;
m = box_neadr_m(:, 2);
mps = box_neadr_m(:, 2) + n_std * box_neadr_var(:, 2);
mms = box_neadr_m(:, 2) - n_std * box_neadr_var(:, 2);
mstr = '$m_{NEADR, y}$';
sstr = ['$m_{NEADR, y} \pm ', num2str(n_std) , '\sigma_{NEADR, y}$'];
tstr = 'Y-Coordinate of BOX NEADR Marker';
plot(m, 'k', 'DisplayName', mstr);
plot(mps, 'r', 'DisplayName', sstr);
plot(mms, 'r', 'HandleVisibility', 'Off');
xlabel('Trials');
ylabel('Position [m]');
title(tstr);
legend;


%% 

n_std = 3;
figure;
sgtitle('Box Markers (before lift) in Human Base Frame');
set(groot,'defaultAxesTickLabelInterpreter','latex'); 
set(groot,'defaulttextinterpreter','latex');
set(groot,'defaultLegendInterpreter','latex');
subplot(4,2,1)
hold on;
m = box_fardl_2_base_m(:, 1);
mps = box_fardl_2_base_m(:, 1) + n_std * box_fardl_2_base_var(:, 1);
mms = box_fardl_2_base_m(:, 1) - n_std * box_fardl_2_base_var(:, 1);
mstr = '$m_{FARDL, x}$';
sstr = ['$m_{FARDL, x} \pm ', num2str(n_std) , '\sigma_{FARDL, x}$'];
tstr = 'X-Coordinate of BOX FARDL Marker';
plot(m, 'k', 'DisplayName', mstr);
plot(mps, 'r', 'DisplayName', sstr);
plot(mms, 'r', 'HandleVisibility', 'Off');
xlabel('Trials');
ylabel('Position [m]');
title(tstr);
legend;

subplot(4,2,2)
hold on;
m = box_fardl_2_base_m(:, 2);
mps = box_fardl_2_base_m(:, 2) + n_std * box_fardl_2_base_var(:, 2);
mms = box_fardl_2_base_m(:, 2) - n_std * box_fardl_2_base_var(:, 2);
mstr = '$m_{FARDL, y}$';
sstr = ['$m_{FARDL, y} \pm ', num2str(n_std) , '\sigma_{FARDL, y}$'];
tstr = 'Y-Coordinate of BOX FARDL Marker';
plot(m, 'k', 'DisplayName', mstr);
plot(mps, 'r', 'DisplayName', sstr);
plot(mms, 'r', 'HandleVisibility', 'Off');
xlabel('Trials');
ylabel('Position [m]');
title(tstr);
legend;


subplot(4,2,3)
hold on;
m = box_fardr_2_base_m(:, 1);
mps = box_fardr_2_base_m(:, 1) + n_std * box_fardr_2_base_var(:, 1);
mms = box_fardr_2_base_m(:, 1) - n_std * box_fardr_2_base_var(:, 1);
mstr = '$m_{FARDR, x}$';
sstr = ['$m_{FARDR, x} \pm ', num2str(n_std) , '\sigma_{FARDR, x}$'];
tstr = 'X-Coordinate of BOX FARDR Marker';
plot(m, 'k', 'DisplayName', mstr);
plot(mps, 'r', 'DisplayName', sstr);
plot(mms, 'r', 'HandleVisibility', 'Off');
xlabel('Trials');
ylabel('Position [m]');
title(tstr);
legend;

subplot(4,2,4)
hold on;
m = box_fardr_2_base_m(:, 2);
mps = box_fardr_2_base_m(:, 2) + n_std * box_fardr_2_base_var(:, 2);
mms = box_fardr_2_base_m(:, 2) - n_std * box_fardr_2_base_var(:, 2);
mstr = '$m_{FARDR, y}$';
sstr = ['$m_{FARDR, y} \pm ', num2str(n_std) , '\sigma_{FARDR, y}$'];
tstr = 'Y-Coordinate of BOX FARDR Marker';
plot(m, 'k', 'DisplayName', mstr);
plot(mps, 'r', 'DisplayName', sstr);
plot(mms, 'r', 'HandleVisibility', 'Off');
xlabel('Trials');
ylabel('Position [m]');
title(tstr);
legend;



subplot(4,2,5)
hold on;
m = box_farur_2_base_m(:, 1);
mps = box_farur_2_base_m(:, 1) + n_std * box_farur_2_base_var(:, 1);
mms = box_farur_2_base_m(:, 1) - n_std * box_farur_2_base_var(:, 1);
mstr = '$m_{FARUR, x}$';
sstr = ['$m_{FARUR, x} \pm ', num2str(n_std) , '\sigma_{FARUR, x}$'];
tstr = 'X-Coordinate of BOX FARUR Marker';
plot(m, 'k', 'DisplayName', mstr);
plot(mps, 'r', 'DisplayName', sstr);
plot(mms, 'r', 'HandleVisibility', 'Off');
xlabel('Trials');
ylabel('Position [m]');
title(tstr);
legend;

subplot(4,2,6)
hold on;
m = box_farur_2_base_m(:, 2);
mps = box_farur_2_base_m(:, 2) + n_std * box_farur_2_base_var(:, 2);
mms = box_farur_2_base_m(:, 2) - n_std * box_farur_2_base_var(:, 2);
mstr = '$m_{FARUR, y}$';
sstr = ['$m_{FARUR, y} \pm ', num2str(n_std) , '\sigma_{FARUR, y}$'];
tstr = 'Y-Coordinate of BOX FARUR Marker';
plot(m, 'k', 'DisplayName', mstr);
plot(mps, 'r', 'DisplayName', sstr);
plot(mms, 'r', 'HandleVisibility', 'Off');
xlabel('Trials');
ylabel('Position [m]');
title(tstr);
legend;

subplot(4,2,7)
hold on;
m = box_neadr_2_base_m(:, 1);
mps = box_neadr_2_base_m(:, 1) + n_std * box_neadr_2_base_var(:, 1);
mms = box_neadr_2_base_m(:, 1) - n_std * box_neadr_2_base_var(:, 1);
mstr = '$m_{NEADR, x}$';
sstr = ['$m_{NEADR, x} \pm ', num2str(n_std) , '\sigma_{NEADR, x}$'];
tstr = 'X-Coordinate of BOX NEADR Marker';
plot(m, 'k', 'DisplayName', mstr);
plot(mps, 'r', 'DisplayName', sstr);
plot(mms, 'r', 'HandleVisibility', 'Off');
xlabel('Trials');
ylabel('Position [m]');
title(tstr);
legend;

subplot(4,2,8)
hold on;
m = box_neadr_2_base_m(:, 2);
mps = box_neadr_2_base_m(:, 2) + n_std * box_neadr_2_base_var(:, 2);
mms = box_neadr_2_base_m(:, 2) - n_std * box_neadr_2_base_var(:, 2);
mstr = '$m_{NEADR, y}$';
sstr = ['$m_{NEADR, y} \pm ', num2str(n_std) , '\sigma_{NEADR, y}$'];
tstr = 'Y-Coordinate of BOX NEADR Marker';
plot(m, 'k', 'DisplayName', mstr);
plot(mps, 'r', 'DisplayName', sstr);
plot(mms, 'r', 'HandleVisibility', 'Off');
xlabel('Trials');
ylabel('Position [m]');
title(tstr);
legend;


%% 

n_std = 3;
figure;
sgtitle('Position of Box w.r.t. Human Wrist during lift');
set(groot,'defaultAxesTickLabelInterpreter','latex'); 
set(groot,'defaulttextinterpreter','latex');
set(groot,'defaultLegendInterpreter','latex');
subplot(1,2,1)
hold on;
m = box_2_wrist_m(:, 1);
mps = box_2_wrist_m(:, 1) + n_std * box_2_wrist_var(:, 1);
mms = box_2_wrist_m(:, 1) - n_std * box_2_wrist_var(:, 1);
mstr = '$m_{BOX2WRI, x}$';
sstr = ['$m_{BOX2WRI, x} \pm ', num2str(n_std) , '\sigma_{BOX2WRI, x}$'];
tstr = 'X-Coordinate of BOX2WRI Vector';
plot(m, 'k', 'DisplayName', mstr);
plot(mps, 'r', 'DisplayName', sstr);
plot(mms, 'r', 'HandleVisibility', 'Off');
xlabel('Trials');
ylabel('Position [m]');
title(tstr);
legend;

subplot(1,2,2)
hold on;
m = box_2_wrist_m(:, 2);
mps = box_2_wrist_m(:, 2) + n_std * box_2_wrist_var(:, 2);
mms = box_2_wrist_m(:, 2) - n_std * box_2_wrist_var(:, 2);
mstr = '$m_{BOX2WRI, y}$';
sstr = ['$m_{BOX2WRI, y} \pm ', num2str(n_std) , '\sigma_{BOX2WRI, y}$'];
tstr = 'Y-Coordinate of BOX2WRI Vector';
plot(m, 'k', 'DisplayName', mstr);
plot(mps, 'r', 'DisplayName', sstr);
plot(mms, 'r', 'HandleVisibility', 'Off');
xlabel('Trials');
ylabel('Position [m]');
title(tstr);
legend;