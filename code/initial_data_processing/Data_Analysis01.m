%DATA_ANALYSIS01 loads the segmented trajectories from the input directory:
%'../../processed_data/step03b_separated_segmentation'
%which are grouped into 'normal' or 'squat', but also into 'lift' and
%'lower' type motions.
%
clear all;
close all;
clc;

%% Add model functions
addpath('../model');

%% Define directories that are in play

input_data_dir = '../../processed_data/step03b_separated_segmentation';
processed_data_dir = '../../processed_data/step04_grouped';
save_graphs_dir_1 = '../../processed_data/graphs/Analysis01';
% if ~exist(processed_data_dir, 'dir'); mkdir(processed_data_dir); end
if ~exist(save_graphs_dir_1, 'dir'); mkdir(save_graphs_dir_1); end

%% Define the keyword of the subdirectories you want to analyze
keyword_dir = 'normal';
% keyword_dir = 'squat';

% keyword_file = 'lift';
keyword_file = 'lower';
%% Check input data directory

% Get all within input_data folder (by default two directories with name '.'
% and '..', so we'll start checking items with index 3)
subject_data = dir(input_data_dir);

% Total number of items within it
num_items = length(subject_data);

% Define structure of all trials
AllTrials = cell(9, 0);

% Go from the first non-default element at index 3
for ii = 3 : num_items
    
    % Get subdirectory name
    subdir_name = subject_data(ii).name;
    
    % Get the ssubdirectory path
    subdir_path = [input_data_dir, '/', subdir_name];
    
    % Get subsbudirectories
    subsubdirs = dir(subdir_path);
    
    % Total number of subitems
    num_sub = length(subsubdirs);
    
    % Go from the first non-default element at index 3
    for jj = 3 : num_sub
        
        % Get subsubdirectory name
        subsubdir_name = subsubdirs(jj).name;
        
        % If the subdirectory doesn't contain the keyword skip it
        if isequal(strfind(subsubdir_name, keyword_dir), [])
            continue
        end
        
        % Get the subsubdirectory path
        subsubdir_path = [subdir_path, '/', subsubdir_name];
        
        % If it does, load file by file
        subsubdir_files = dir(subsubdir_path);
        
        % Number of subsubdirectory files
        num_subsub = length(subsubdir_files);
        
        % Go file by file
        for kk = 3 : num_subsub
            
            % Get the filename
            subsubdir_filename = subsubdir_files(kk).name;
            
            % If the file contains the file keyword and it's a .mat file
            if ~isequal(strfind(subsubdir_filename, keyword_file), []) && endsWith(subsubdir_filename, '.mat')
                
                % Get the file path
                subsubdir_filepath = [subsubdir_path, '/', subsubdir_filename];
                
                % Load the file
                load(subsubdir_filepath);
                
                % Get name without extension
                just_the_name = strrep(subsubdir_filename, '.mat', '');

                % Get the location of underscores
                loc_underscores = strfind(just_the_name, '_');

                % Get the set number
                set_number = just_the_name(1 : loc_underscores(1) - 1);

                % Get the rep number
                rep_number = just_the_name(loc_underscores(1) + 1 : loc_underscores(2) - 1);

                % Get the motion type
                motion_type = just_the_name(loc_underscores(2) + 1 : end);

                
                % Store the info
                AllTrials(:, end+1) = {subdir_name;
                 subsubdir_name;
                 set_number;
                 rep_number;
                 motion_type;
                 eval(['qr_', keyword_file]);
                 eval(['Markersr_', keyword_file]);
                 eval(['Forceplater_', keyword_file]);
                 eval(['modelParamr_', keyword_file])};
            end
        end
    end
end


%% Process data

% Define the sampling time
Ts = 0.01;

% Prealocate vector of duration of trials
t_trial = zeros(1, size(AllTrials, 2));

% Extract trial duration
for ii = 1 : size(AllTrials, 2)
    t_trial(ii) = size(AllTrials{6, ii}, 2) * Ts;
end

% Draw a histogram of the mean time required to perform the lifting motion
figure;
hold on;
h = histogram(t_trial, 'NumBins', 15, 'DisplayName', ['Trial Duration']);
mean_t_trial = mean(t_trial);  % Mean of the time required
medi_t_trial = median(t_trial); % Median of the time required
plot([mean_t_trial mean_t_trial], [0 max(h.Values)], 'r', 'LineWidth', 5, 'DisplayName', 'Mean');
plot([medi_t_trial medi_t_trial], [0 max(h.Values)], 'LineWidth', 5, 'DisplayName', 'Median');
legend;
xlabel('Duration [s]');
ylabel('Number of occurences');


% Number of samples we'll use for all the trajectories when resampling
Ns = round(medi_t_trial / Ts);

% Prealocate structure to store all resampled trials
AllTrajectories = zeros(6, Ns, size(AllTrials, 2));

% Treat the trajectories of all trials
for kk = 1 : size(AllTrials, 2)
    % Resample with the number of samples Ns, using a 1st order filter
    AllTrajectories(:, :, kk) = resample(AllTrials{6, kk}', Ns, size(AllTrials{6, kk}, 2), 5)';
    
%     % Animate unusually long trajectories
%     if size(AllTrials{6, kk}, 2)*Ts > medi_t_trial*1.5
%         Animate_nDOF(AllTrials{6, kk}, eval(['modelParamr_', keyword_file]).L, 0.01)
%         disp(AllTrials(1:4, kk)')
%         pause;
%         close;
%     end
end
% disp('blabla')

% Get the mean
MeanTrajectory = mean(AllTrajectories, 3);
StdTrajectory = std(AllTrajectories, 0, 3);

% Get the normalized time vector
t_norm = linspace(0, 1, Ns);

% Number of rows and columns of the figure
figcols = 2;
figrows = 3;

% Joint names
Joints = {'Ankle', 'Knee', 'Hip', 'Back', 'Shoulder', 'Elbow'};

% Draw the mean +- std for the trajectories
numstddev = 1;  % How much standard deviations to draw
figure;
sgtitle({['Mean trajectories and standard deviations for ', keyword_dir, ' ', keyword_file, 'ing motion']});
for ii = 1 : figrows
    for jj = 1 : figcols
        
        % Current joint/plot
        curr = jj + (ii-1)*figcols;
        
        % Create subplot grid in row major order
        subplot(figrows, figcols, curr)
        hold on;
        
        % Plot mean joint profile as well as std
        x = t_norm;
        y = MeanTrajectory(curr,:);
        y1 = MeanTrajectory(curr,:) + numstddev*StdTrajectory(curr, :);
        y2 = MeanTrajectory(curr,:) - numstddev*StdTrajectory(curr, :);
        plot(x, y1, 'k', 'DisplayName', ['+-', num2str(numstddev, '%d'), 'std']);
        plot(x, y2, 'k', 'HandleVisibility', 'Off');
        patch([x fliplr(x)], [y1 fliplr(y2)], [220,220,220]/256, 'DisplayName', ['+-', num2str(numstddev, '%d'), 'std-region'])
        plot(x, y, 'LineWidth', 2, 'DisplayName', ['Mean']);
        ylim([-pi, pi])
        if strcmp(Joints{curr}, 'Shoulder'); ylim([0, 2*pi]); end
        
        % Labels
        xlabel('Normalized Time');
        ylabel([Joints{curr} ' angle [rad]']);
        title([Joints{curr} ' joint trajectory']);
        legend('Location', 'Best');
    end
end

%% Plot per subject

% Subjects and their figures (list and struct)
[subjs, indicesTrials, indicesSubjs] = unique(AllTrials(1, :));
PerSubjectTrials = cell(1, length(subjs));
PerSubjectTrajectories = cell(1, length(subjs));
PerSubjectMeans = cell(1, length(subjs));
PerSubjectStds = cell(1, length(subjs));

% Per set
PerSetTrials = cell(1, length(subjs));
PerSetTrajectories = cell(1, length(subjs));
PerSetMeans = cell(1, length(subjs));
PerSetStds = cell(1, length(subjs));

% Reset the number of std deviations
numstddev = 3;

% For each subject
for ss = 1 : length(subjs)
% for ss = 1 : 1
    
    % Get all the Trials and Trajectories corresponding to that subject
    PerSubjectTrials{ss} = AllTrials(:, indicesSubjs == ss);
    PerSubjectTrajectories{ss} = AllTrajectories(:, :, indicesSubjs == ss);
    
    % Get the mean trajectories and standard deviations
    PerSubjectMeans{ss} = mean(PerSubjectTrajectories{ss}, 3);
    PerSubjectStds{ss} = std(PerSubjectTrajectories{ss}, 0, 3);
    
    % Get the unique sets
    [sets, indicesSubjTrials, indicesSets] = unique(PerSubjectTrials{ss}(3, :));
    
    % Prealocate
    PerSetTrials{ss} = cell(1, length(sets));
    PerSetTrajectories{ss} = cell(1, length(sets));
    PerSetMeans{ss} = cell(1, length(sets));
    PerSetStds{ss} = cell(1, length(sets));
    
    % For each separate set
    for tt = 1 : length(sets)
%     for tt = 1 : 1
        % Get all the Trials and Trajectories corresponding to that set and
        % that subject
        PerSetTrials{ss}{tt} = PerSubjectTrials{ss}(:, indicesSets == tt);
        PerSetTrajectories{ss}{tt} = PerSubjectTrajectories{ss}(:, :, indicesSets == tt);
        
        % Get the Mean trajectories and standard deviations
        PerSetMeans{ss}{tt} = mean(PerSetTrajectories{ss}{tt}, 3);
        PerSetStds{ss}{tt} = std(PerSetTrajectories{ss}{tt}, 0, 3);
        
        % Number of rows and columns of the figure
        figcols = 2;
        figrows = 3;

        % Joint names
        Joints = {'Ankle', 'Knee', 'Hip', 'Back', 'Shoulder', 'Elbow'};

        % Get current subject name
        curr_subj_name = strrep(subjs{ss}, 'subject_', '');
        curr_subj_name(1) = upper(curr_subj_name(1));
        
        % Get the current set
        curr_set = sets{tt};

        % Create the plot
        figs.(curr_subj_name).(curr_set) = figure('Name', [curr_subj_name, ' : ', curr_set]);

        % Subgrid title
        sgtitle({['Mean trajectories and standard deviations for ', keyword_dir, ' ', keyword_file, 'ing motion']; ['for subject ', strrep(curr_subj_name, 'subject_', ''), ' : ', curr_set]});

        for ii = 1 : figrows
            for jj = 1 : figcols

                % Current joint/plot
                curr = jj + (ii-1)*figcols;

                % Create subplot grid in row major order
                subplot(figrows, figcols, curr)
                hold on;

                % Plot mean joint profile as well as std
                x = t_norm;
                y = PerSetMeans{ss}{tt}(curr,:);
                y1 = PerSetMeans{ss}{tt}(curr,:) + numstddev*PerSetStds{ss}{tt}(curr, :);
                y2 = PerSetMeans{ss}{tt}(curr,:) - numstddev*PerSetStds{ss}{tt}(curr, :);
                plot(x, y1, 'k', 'DisplayName', ['+-', num2str(numstddev, '%d'), 'std']);
                plot(x, y2, 'k', 'HandleVisibility', 'Off');
                patch([x fliplr(x)], [y1 fliplr(y2)], [220,220,220]/256, 'DisplayName', ['+-', num2str(numstddev, '%d'), 'std-region']);
                plot(x, y, 'LineWidth', 2, 'DisplayName', ['Mean']);
                plot(x, squeeze(PerSetTrajectories{ss}{tt}(curr, :, :)), '--', 'HandleVisibility', 'Off', 'LineWidth', 0.5);
                plot(x, MeanTrajectory(curr, :), 'm', 'LineWidth', 2, 'DisplayName', 'Mean-All');
                ylim([-pi, pi])
                if strcmp(Joints{curr}, 'Shoulder'); ylim([0, 2*pi]); end

                % Labels
                xlabel('Normalized Time');
                ylabel([Joints{curr} ' angle [rad]']);
                title([Joints{curr} ' joint trajectory']);
                legend('Location', 'Best');
                
                % Enlarge figure to full screen.
                set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
                
                % Save figure
                savedir = [save_graphs_dir_1, '/', subjs{ss}];
                if ~exist(savedir); mkdir(savedir); end
                
%                 saveas(gcf, [savedir '/', curr_set, '.png']);
            end
        end
    end
end
