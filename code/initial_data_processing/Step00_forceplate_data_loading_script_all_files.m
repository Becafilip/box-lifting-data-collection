%FORCEPLATE_DATA_LOADING_SCRIPT_ALL_FILES is a script that transforms all
%the csv files within the input directory (to set within script, e.g.
%subjects_data) that contain the keyword 'forceplate' to .mat files.
clear all;
close all;
clc;

%% Directories

% Keyword for forceplate data
keyword = 'forceplate';

% Input directory 
input_directory = '../../raw_data/subjects_data';

% Get subdirectories
input_subdirectories = dir(input_directory);

% Keep only the files which are directories
input_subdirectories = input_subdirectories([input_subdirectories.isdir]);

% Remove the default '.' and '..' directories
input_subdirectories = input_subdirectories(3:end);

% For each subdirectory
for iisub = 1 : length(input_subdirectories)
    
    % Current subdirectory name
    curr_subdir = [input_directory, '/', input_subdirectories(iisub).name];
    
    % Current subdirectory files
    curr_files = dir(curr_subdir);
    
    % Indices to keep
    ind2keep = [];
    
    % For each current file
    for ii = 1 : length(curr_files)
        % One who's name contains the keyword and .csv
        if ~isempty(strfind(curr_files(ii).name, '.csv'))
            if ~isempty(strfind(curr_files(ii).name, keyword))
                % Store its index
                ind2keep = [ind2keep, ii];
            end
        end
    end
    
    % Extract files which fit the criteria
    curr_files = curr_files(ind2keep);
               
    % For each file
    for numfilname = 1 : length(curr_files)

        % Input filename
        input_filename = [curr_subdir, '/', curr_files(numfilname).name];
        % Read the whole CSV, get the numeric, text and raw data
        [n,t,r] = xlsread(input_filename);

        % Get the row numbers where the numeric data starts
        numDataIndex = find(n(:, 1) == 0, 1, 'first');
        n = n(numDataIndex:end, :);

        % Store Forceplate data

        Forceplate = [];    

        Forceplate.Forces = n(1:end, 4:6);
        Forceplate.Moments = n(1:end, 7:9);
        Forceplate.COP = n(1:end, 10:12);

        % Save output

        % Generate output filename by replacing .csv by .mat
        output_filename = strrep(input_filename, '.csv', '.mat');

        % Save the output data
        save(output_filename, 'Forceplate');
    end
end