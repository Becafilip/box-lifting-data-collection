function P = gaussian_prob(m, sig, x, varargin)
%GAUSSIAN_PROB evaluates the probability of gaussian with given mean and
%standard deviation at a vector of points.
%   P = GAUSSIAN_PROB(m, sig, x) returns the vector of computed
%   probabilites of a gaussian with given mean and standard deviation at a 
%   vector of points.
%   
%   P = GAUSSIAN_PROB(m, sig, x, stol, tol) returns the vector of computed
%   probabilites of a gaussian with given mean and standard deviation at a 
%   vector of points. If sig=0 (sig<stol), returns one for samples where 
%   |x-m| < tol.
%   Defaults: stol = 1e-10
%             tol  = 1e-3

% If 4 arguments
if nargin > 3
    stol = varargin{1};
    tol = varargin{2};
else
    stol = 1e-10;
    tol = 1e-3;
end

% Check if sigma is 0
if sig < stol
    P = zeros(size(x));
    P(abs(x-m) < tol) = 1;
    return
end

% Otherwise
P = exp(-0.5*((x-m)/sig).^2) ./ (sig * sqrt(2*pi));

end
