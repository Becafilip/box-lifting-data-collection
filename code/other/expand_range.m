function [low, upp] = expand_range(l, u, c)
%EXPAND_RANGE expands the static range symetrically between two values by a
%given coefficient.
%   [low, up] = EXPAND_RANGE(l, u, c) returns the newly computed lower and
%   upper bounds, from given lower and upper bounds and the coefficient of
%   expansion or reduction.

% Difference
d = u - l;
% Midpoint
m = (u + l) / 2;
% Expand difference
e = c * d;
% New points
low = m - e/2;
upp = m + e/2;

end
